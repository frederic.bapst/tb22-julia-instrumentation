using Graphs, GraphRecipes
using Plots: plot, png

include("Utils.jl")

"""Lock used in react function to handle asynchronous code"""
reactLock = ReentrantLock()

"""React to an anomaly with respect to the configuration."""
function react(meta::Metadata, message::String)::Nothing
    st = stacktrace()
    # logs the message if enabled in the config
    if meta.config.reaction.log
        reactLog(message, st)
    end
    # If needed, we retrieve the reaction in an internal list, we
    # need a lock to ensure it works with asynchronous code.
    if meta.config.reaction.graph.enabled || meta.config.reaction.report
        lock(reactLock)
        key = (message,st)
        if !haskey(meta.inner.reactions, key)
            meta.inner.reactions[key] = 1
        else
            meta.inner.reactions[key] += 1
        end
        unlock(reactLock)
    end
    return
end

"""Log the message to stderr.
Used when reaction logs are enabled in the config."""
function reactLog(message::String, st::Base.StackTrace)::Nothing
    frame = reasonableStackFrame(st)
    file = string(frame.file)
    line = frame.line
    @warn message _module=nothing _file=file _line=line
end

"""Print a report of all the triggered reactions.
Used when reaction reports are enabled in the config."""
function reactReport(meta::Metadata)::Nothing
    reactions = meta.inner.reactions
    d = Dict()
    # We start by creating a "double" dict of message => (lineAndFile => numberOfTimes)
    # so we can use it to print our report.
    for ((msg, st), n)  in reactions
        frame = reasonableStackFrame(st)
        frameString = stackFrameString(frame)
        if !haskey(d, msg)
            d[msg] = Dict()
        end
        if !haskey(d[msg], frameString)
            d[msg][frameString] = n
        else
            d[msg][frameString] += n
        end
    end
    # We then create a string representing our report
    report =  "COJAC Anomalies Report\n"
    report *= "----------------------------\n"
    for (msg, anomalies) in d
        report *= msg * "\n"
        for (pos, num) in anomalies
            report *= "\t$num times -> $pos\n"
        end
    end
    report *= "----------------------------"
    # Finally we can log it
    @info report
end

"""Create a graph representing all anomalies stacktraces and save it in a file.
Used when reaction graphs are enabled in the config."""
function reactGraph(meta::Metadata)::Nothing
    reactions = meta.inner.reactions
    config = meta.config.reaction.graph
    g = SimpleDiGraph() # will contain the connexions beween edges and vertices
    vertices = [] # will contain all our vertices names
    verticesColors = [] # "file and line" vertices are white while "message" vertices are blue
    edgesLabels = Dict() # will contain the label for each edge
    for ((msg, st), n) in reactions
        # We create the first vertice, representing the message
        if !(msg in vertices)
            push!(vertices, msg)
            push!(verticesColors, :dodgerBlue)
            add_vertex!(g)
        end
        prev = msg # we save the name of the previous vertice in so we can connect to it with the next
        for frame in st
            # Ininteresting stackframes are those related to Cojac, Cassette, Base julia or external libraries
            if !isStackFrameInteresting(frame)
                continue
            end
            frameString = stackFrameString(frame)
            # We create the vertice for this particular frame
            if !(frameString in vertices)
                push!(vertices, frameString)
                push!(verticesColors, :white)
                add_vertex!(g)
            end
            # We then connect it to the previous one
            if !isnothing(prev)
                # We find both vertices indices in the graph by their names
                src = findfirst(isequal(frameString), vertices)
                dst = findfirst(isequal(prev), vertices)
                if src != dst
                    # We then name the label and add the edge
                    if !haskey(edgesLabels, (src, dst))
                        edgesLabels[(src, dst)] = 0
                    end
                    edgesLabels[(src, dst)] += n
                    add_edge!(g, src, dst)
                end
            end
            # Lastly we set the vertice as the previous one
            prev = frameString
        end
    end
    # if no edges are found we have nothing to do
    if isempty(edgesLabels)
        return
    end
    # We then generate the graph and save the image
    fullGraph = graphplot(
        g, names=vertices, nodecolor=verticesColors,
        edgelabel=edgesLabels, nodeshape=:rect,
        size=config.size
    )
    png(fullGraph, config.file)
end