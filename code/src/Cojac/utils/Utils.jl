"""
Try to find if a particular stackFrame is "interesting" to show the user.
An "interesting stackFrame" would be one that is written by the user, meaning
not from Julia Base modules or an external library.
There's no really reliable way to do it, so it's a bit error prone.
"""
function isStackFrameInteresting(frame::Base.StackFrame)::Bool
    file = string(frame.file)
    # starting with "." means it's from Base package ans containing "julia" means it's from an external package
    return !startswith(file, ".") && !contains(file, "Cojac") && !contains(file, "julia")
end

"""
Find a stackframe that seems to be one that the user has written (so not from an external module).
"""
function reasonableStackFrame(st::Base.StackTrace)::Base.StackFrame
    for frame in st
        if isStackFrameInteresting(frame)
            return frame
        end
    end
    return st[end]
end

"""Retrieve a the file and line of a stackFrame and return it in a string form"""
function stackFrameString(frame::StackTraces.StackFrame)::String
    file = split(string(frame.file),"/")[end]
    return "$file:$(frame.line)"
end

"""Map a function to type parameters
Used to unwrap parameters for wrappers
"""
function mapParams(func::Function, type::Type)
    if length(type.parameters) == 0
        return type
    end
    params = map(func, type.parameters)
    newType = type.name.wrapper
    return newType{params...}
end