"A union type of all arithmetic operators"
ArithmeticOperators = Union{typeof(+),typeof(-),typeof(*),typeof(/),typeof(\),typeof(÷),typeof(^),typeof(rem)}

"A union type of all comparison operators"
ComparisonOperators = Union{typeof(<),typeof(>),typeof(<=),typeof(>=),typeof(!=),typeof(==)}

"A union type of all numeric operators"
NumericOperators = Union{ArithmeticOperators, ComparisonOperators}