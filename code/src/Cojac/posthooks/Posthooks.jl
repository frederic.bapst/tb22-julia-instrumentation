include("check/Check.jl")

"""The posthook calls the checks if there are enabled in the configuration"""
function posthook(meta::Metadata, res, func, args...)::Nothing
    if meta.config.check.enabled
        check(meta, func, res, args...)
    end
end

"""Lock used in our posthook on schedule to handle asynchronous code"""
scheduleLock = ReentrantLock()

"""A special hook on "schedule" to work with asynchronous code.
The "schedule" function is used internally to run an asynchronous task. Here, we intercept the task and register
it in a interal list. We can use that list later to wait on all the task and know when they're all finished.
The lock is used so that it works even if tasks are schedule from inside another task."""
function posthook(meta::Metadata, t::Task, ::typeof(schedule), args...)::Nothing
    lock(scheduleLock)
    push!(meta.inner.tasks, t)
    unlock(scheduleLock)
    return
end