include("IntegersCheck.jl")
include("FloatsCheck.jl")

"""Most general case, nothing to check."""
check(args...) = nothing

"""Check anomalies on all numeric operators.
We start by unwrapping the arguments because operators aren't directly implemented on wrapper but are instead overdubbed
to change their behaviors. The overdub don't take effect in posthooks so we have to work on the number value."""
function check(meta::Metadata, func::NumericOperators, r::Number, args::Number...)::Nothing
    r = unwrap(r)
    args = unwrap(args)
    checkInteger(meta, func, r, args...)
    checkFloat(meta, func, r, args...)
end