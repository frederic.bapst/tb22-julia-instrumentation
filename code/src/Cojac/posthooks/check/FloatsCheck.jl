checkFloat(args...) = nothing

"""Call every checks for float numbers"""
function checkFloat(meta::Metadata, func, r, a::Number, b::Number)::Nothing
    checkAbsorption(meta, func, r, a, b)
    checkCancellation(meta, func, r, a, b)
    checkComparison(meta, func, r, a, b)
    checkUnderflow(meta, func, r, a, b)
    checkNaN(meta, func, r, a, b)
end

################
#  Absorption  #
################

"""Most general case, nothing to check"""
checkAbsorption(args...) = nothing

"""Check if an addition of floats results in an absorption"""
function checkAbsorption(meta::Metadata, ::typeof(+), r::AbstractFloat, a::Number, b::Number)::Nothing
    if isinf(a) || isinf(b)
        return
    end
    if (b != 0 && r == a) || (a != 0 && r == b)
        react(meta, "Absorption: FADD")
    end
end

"""Check if a substraction of floats results in an absorption"""
function checkAbsorption(meta::Metadata, ::typeof(-), r::AbstractFloat, a::Number, b::Number)::Nothing
    if isinf(a) || isinf(b)
        return
    end
    if b != 0 && r == a || a != 0 && r == -b
        react(meta, "Absorption: FSUB")
    end
end

"""Check if a remainder of floats results in an absorption"""
function checkAbsorption(meta::Metadata, ::typeof(%), r::AbstractFloat, a::AbstractFloat, b::Number)::Nothing
    if eps(a) > abs(b)
        react(meta, "Absorption: FREM")
    end
end

################
# Cancellation #
################

"Most general case, nothing to check"
checkCancellation(args...) = nothing

"""Check if an addition of floats results in a cancellation"""
function checkCancellation(meta::Metadata, ::typeof(+), r::AbstractFloat, a::AbstractFloat, b::Number)::Nothing
    threshold = meta.config.check.cancellationThreshold
    if r != 0 && abs(r) <= threshold * eps(a)
        react(meta, "Cancellation: FADD")
    end
end

"""Check if a substraction of floats results in a cancellation"""
function checkCancellation(meta::Metadata, ::typeof(-), r::AbstractFloat, a::AbstractFloat, b::Number)::Nothing
    threshold = meta.config.check.cancellationThreshold
    if r != 0 && abs(r) <= threshold * eps(a)
        react(meta, "Cancellation: FSUB")
    end
end

################
#  Comparison  #
################

"Most general case, nothing to check"
checkComparison(args...) = nothing


"""Check if a comparison of floats is questionnable"""
function checkComparison(meta::Metadata, ::ComparisonOperators, r::Bool, a::AbstractFloat, b::Number)::Nothing
    threshold = meta.config.check.comparisonThreshold
    if r != 0 && abs(a - b) <= threshold * eps(a)
        react(meta, "Comparing very close: DCMP");
    end
end

################
#  Underflow   #
################

"Most general case, nothing to check"
checkUnderflow(args...) = nothing

"""Check if a multiplication of floats results in an underflow"""
function checkUnderflow(meta::Metadata, ::typeof(*), r::AbstractFloat, a::Number, b::Number)::Nothing
    if r == 0 && a != 0 && b != 0
        react(meta, "Underflow: DMUL")
    end
end

"""Check if a division of floats results in an underflow"""
function checkUnderflow(meta::Metadata, ::typeof(/), r::AbstractFloat, a::Number, b::Number)::Nothing
    if r == 0 && a != 0
        react(meta, "Underflow: DDIV")
    end
end

"""Check if a power of floats results in an underflow"""
function checkUnderflow(meta::Metadata, ::typeof(^), r::AbstractFloat, a::Number, b::Number)::Nothing
    if r == 0 && a != 0
        react(meta, "Underflow: DPOW")
    end
end

#################
# Underflow/NaN #
#################

"Most general case, nothing to check"
checkNaN(args...) = nothing

"""Check if a float operation results in an overflow or a NaN"""
function checkNaN(meta::Metadata, op::ArithmeticOperators, r::AbstractFloat, a::Number, b::Number)::Nothing
    if isfinite(a) && isfinite(b) && !isfinite(r)
        react(meta, "$r float operation: $op")
    end
end