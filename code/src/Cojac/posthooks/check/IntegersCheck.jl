"""Most general case, nothing to check"""
checkInteger(args...) = nothing

############
# INTEGERS #
############

"""Check if an addition of integers results in an overflow"""
function checkInteger(meta::Metadata, ::typeof(+), r::Integer, a::Integer, b::Integer)::Nothing
    if ((a ⊻ r) & (b ⊻ r)) < 0
        react(meta, "Overflow : IADD")
    end
end

"""Check if a substractions of integers results in an overflow"""
function checkInteger(meta::Metadata, ::typeof(-), r::Integer, a::Integer, b::Integer)::Nothing
    if ((a ⊻ b) & (a ⊻ r)) < 0
        react(meta, "Overflow : ISUB")
    end
end

"""Check if a negation of an integer results in an overflow"""
function checkInteger(meta::Metadata, ::typeof(-), r::Integer, a::Integer)::Nothing
    if r == a
        react(meta, "Overflow : INEG")
    end
end

"""Check if a multiplication of integers results in an overflow"""
function checkInteger(meta::Metadata, ::typeof(*), r::Integer, a::Integer, b::Integer)::Nothing
    if b==0 && r== 0 
        return
    end
    if r//b != a
        react(meta, "Overflow : IMUL")
    end
end

############
# UNSIGNED #
############

# For unsigned integers, additions and substractions cannot be checked in the same way that signed integers

"""Check if an addition of unsigned integers results in an overflow"""
function checkInteger(meta::Metadata, ::typeof(+), r::Unsigned, a::Unsigned, b::Unsigned)::Nothing
    if r < a
        react(meta, "Overflow : IADD")
    end
end

"""Check if a substraction of unsigned integers results in an overflow"""
function checkInteger(meta::Metadata, ::typeof(-), r::Unsigned, a::Unsigned, b::Unsigned)::Nothing
    if r > a
        react(meta, "Overflow : ISUB")
    end
end