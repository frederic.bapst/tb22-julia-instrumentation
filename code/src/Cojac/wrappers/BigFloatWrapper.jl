struct BigFloatWrapper{T<:Real} <:AbstractFloat
    value::BigFloat
end

function BigFloatWrapper{T}(n::Real, precision::Int) where {T<:Real}
    return BigFloatWrapper{T}(BigFloat(n, precision))
end

function BigFloatWrapper{T}(n::BigFloatWrapper{S}) where {T<:Real,S<:Real}
    return BigFloatWrapper{T}(n.value)
end

Base.show(io::IO, x::BigFloatWrapper) = Base.show(io, x.value)

"""Handle promotion"""
Base.promote_rule(::Type{BigFloatWrapper{T}}, ::Type{BigFloat}) where T = BigFloat
Base.promote_rule(::Type{BigFloatWrapper{T}}, other::Type{S}) where {T,S<:Real} = BigFloatWrapper{promote_type(T, other)}
Base.promote_rule(::Type{BigFloatWrapper{T}},
    ::Type{BigFloatWrapper{S}}) where {T,S<:Real}= BigFloatWrapper{promote_type(T, S)}

"""Workaround for matrix multiplication that still use promote_op"""
Base.promote_op(::Function, ::Type{BigFloatWrapper{T}}, 
    ::Type{BigFloatWrapper{S}}) where {T,S} = BigFloatWrapper{promote_type(T, S)}
    

"""Handle conversion from/to BigFloatWrapper"""
(::Type{T})(n::BigFloatWrapper) where T<:Real = begin
    if BigFloatWrapper <: T
        return n
    else
        return T(n.value)
    end
end
Base.convert(::Type{T}, n::BigFloatWrapper) where T<:Real = T(n)
"""BigFloat needs special conversion to keep the precision"""
BigFloat(n::BigFloatWrapper{T}) where T<:Real = n.value
BigFloat(n::BigFloatWrapper{T}, precision::Int) where T<:Real = BigFloat(n.value, precision)

function wrap(ctx::CojacCtx, ::Type{BigFloatWrapper}, value::AbstractFloat)
    config = ctx.metadata.config.wrappers.bigFloat
    if isa(value, BigFloatWrapper)
        return value
    end
    return BigFloatWrapper{typeof(value)}(value, config.precision)
end
function wrap(::CojacCtx, ::Type{BigFloatWrapper}, value::Type{T}) where {T<:AbstractFloat}
    return BigFloatWrapper{lossyUnwrap(value)}
end

"""Unwrap the BigFloatWrapper into a BigFloat"""
unwrap(::Type{BigFloatWrapper{T}}) where T<:Real = BigFloat

"""Unwrap the BigFloatWrapper{T} into type T"""
lossyUnwrap(::Type{BigFloatWrapper{T}}) where T<:Real = T

function computeWithPrecision(op::Function, a::Real, b::Real, precision::Int, resultType::DataType)
    return setprecision(precision) do
        res = op(unwrap(a), unwrap(b))
        return resultType(res, precision)
    end
end

for op in [:+,:-,:*,:/,:÷,:\,:^,:%]
    eval(quote
        Base.$op(a::BigFloatWrapper, b::Bool) = computeWithPrecision($op, a, b, a.value.prec, typeof(a))
        Base.$op(a::Bool, b::BigFloatWrapper) = computeWithPrecision($op, a, b, b.value.prec, typeof(b))
        Base.$op(a::BigFloatWrapper, b::Integer) = computeWithPrecision($op, a, b, a.value.prec, typeof(a))
        Base.$op(a::Integer, b::BigFloatWrapper) = computeWithPrecision($op, a, b, b.value.prec, typeof(b))
        Base.$op(a::BigFloatWrapper, b::BigFloatWrapper) = begin
            type = Base.promote_typeof(a, b)
            precision = max(a.value.prec, b.value.prec)
            return computeWithPrecision($op, a, b, precision, type)
        end
    end)
end

Base.:+(a::BigFloatWrapper) = a
Base.:-(a::BigFloatWrapper) = 0-a

for op in [:<,:>,:<=,:>=,:!=,:(==)]
    eval(quote
        Base.$op(a::BigFloatWrapper, b::BigFloatWrapper) = begin
            return $op(unwrap(a), unwrap(b))
        end
    end)
end