struct BigIntWrapper{T<:Integer} <:Integer
    value::BigInt
    bitwiseUnwrap::Bool
end

function BigIntWrapper{T}(n::Integer) where {T<:Integer}
    return BigIntWrapper{T}(n, true)
end

function BigIntWrapper{T}(n::BigIntWrapper{S}) where {T<:Integer,S<:Integer}
    return BigIntWrapper{T}(n.value, n.bitwiseUnwrap)
end

Base.show(io::IO, x::BigIntWrapper) = Base.show(io, x.value)

"""Handle promotion"""
Base.promote_rule(::Type{BigIntWrapper{T}}, ::Type{BigInt}) where T = BigInt
Base.promote_rule(::Type{BigIntWrapper{T}}, other::Type{S}) where {T,S<:Integer} = BigIntWrapper{promote_type(T, other)}
Base.promote_rule(::Type{BigIntWrapper{T}}, other::Type{S}) where {T,S<:Real} = promote_type(T, other)
Base.promote_rule(::Type{BigIntWrapper{T}},
    ::Type{BigIntWrapper{S}}) where {T,S<:Integer}= BigIntWrapper{promote_type(T, S)}

"""Handle conversion from/to BigIntWrapper"""
(::Type{T})(n::BigIntWrapper) where T<:Integer = begin
    if BigIntWrapper <: T
        return n
    else
        # We convert with respect to wraparound
        upper = typemax(T)
        lower = typemin(T)
        delta = BigInt(upper) - lower + 1
        modulo = (n.value-lower)%delta
        res = (modulo>=0) ? lower+modulo : upper+1+modulo
        return T(res)
    end
end
(::Type{T})(n::BigIntWrapper) where T<:Real = begin
    if BigIntWrapper <: T
        return n
    else
        return T(n.value)
    end
end
Base.convert(::Type{T}, n::BigIntWrapper) where T<:Real = T(n)
"""BigInt needs special conversion to avoid ambiguity with BigInt(::Integer)"""
BigInt(n::BigIntWrapper{T}) where T<:Integer = n.value
Integer(n::BigIntWrapper{T}) where T<:Integer = n

wrap(::CojacCtx, ::Type{BigIntWrapper}, value::Bool) = value
wrap(::CojacCtx, ::Type{BigIntWrapper}, value::Type{Bool}) = value
function wrap(ctx::CojacCtx, ::Type{BigIntWrapper}, value::Integer)
    if isa(value, BigIntWrapper)
        return value
    end
    config = ctx.metadata.config.wrappers.bigInt
    return BigIntWrapper{typeof(lossyUnwrap(value))}(value, config.bitwiseUnwrap)
end
function wrap(::CojacCtx, ::Type{BigIntWrapper}, value::Type{T}) where {T<:Integer}
    return BigIntWrapper{lossyUnwrap(value)}
end

"""Unwrap the BigIntWrapper into a BigInt"""
unwrap(::Type{BigIntWrapper{T}}) where T<:Integer = BigInt

"""Unwrap the BigIntWrapper{T} into type T"""
lossyUnwrap(::Type{BigIntWrapper{T}}) where T<:Integer = T


for op in [:+,:-,:*,:÷,:^,:%]
    eval(quote
        Base.$op(a::BigIntWrapper, b::BigIntWrapper) = begin
            type = Base.promote_typeof(a, b)
            bitwiseUnwrap = a.bitwiseUnwrap || b.bitwiseUnwrap
            res = $op(unwrap(a), unwrap(b))
            return type(res, bitwiseUnwrap)
        end
    end)
end

Base.:+(a::BigIntWrapper) = a
Base.:-(a::BigIntWrapper) = 0-a

for op in [:/,:\,:<,:>,:<=,:>=,:!=,:(==)]
    eval(quote
        Base.$op(a::BigIntWrapper, b::BigIntWrapper) = begin
            return $op(unwrap(a), unwrap(b))
        end
    end)
end

for op in [:~,:&,:|,:⊻,:⊼,:⊽,:>>>,:>>,:<<]
    eval(quote
        Base.$op(args::BigIntWrapper...) = begin
            type = Base.promote_typeof(args...)
            if any(a -> a.bitwiseUnwrap, args)
                args = map(lossyUnwrap, args)
            else
                args = map(unwrap, args)
            end
            res = $op(args...)
            return type(res)
        end
    end)
end

"""hvcat and typed_hvcat explicitely need Int64 as their first argument"""
Base.:hvcat(rows::Tuple{BigIntWrapper,BigIntWrapper}, values...) = hvcat(Tuple{Int64,Int64}(rows), values...)
Base.:typed_hvcat(type::DataType, rows::Tuple{BigIntWrapper,BigIntWrapper}, values...) =
    Base.typed_hvcat(type, Tuple{Int64,Int64}(rows), values...)