include("BigFloatWrapper.jl")
include("BigIntWrapper.jl")
include("BlankWrapper.jl")

"""If unwrap is not a DataType, we unwrap variable type and then use "convert" to convert it
to the unwrapped type."""
unwrap(arg) = convert(unwrap(typeof(arg)), arg)
"""If Type is an array (includes vectors and matrices) or a tuple, we unwrap all their content."""
unwrap(arg::Union{Array,Tuple}) = map(unwrap, arg)
"""If arg is a generic DataType, we unwrap all its parameters."""
unwrap(type::DataType) = mapParams(unwrap, type)

"""Same that for the "unwrap" function."""
lossyUnwrap(arg::Union{Array,Tuple}) = map(lossyUnwrap, arg)
lossyUnwrap(arg) = convert(lossyUnwrap(typeof(arg)), arg)
lossyUnwrap(type::DataType) = mapParams(lossyUnwrap, type)

"""Generic wrap for every type that aren't wrappers, we simply return the argument."""
wrap(::CojacCtx, ::Any, arg) = arg
"""For unions and tuples we wrap all their content"""
wrap(ctx::CojacCtx, wrapper, arg::Union{Array,Tuple}) = map(a -> wrap(ctx, wrapper, a), arg)

"""Verify is any wrapper is currently activated"""
function isWrapped(config::WrappersConfig)
    return config.bigFloat.enabled || config.bigInt.enabled || config.blank
end

"""Call by every function, we wrap it if needed"""
function wrappers(ctx::CojacCtx, func::Function, args...)
    config::WrappersConfig = ctx.metadata.config.wrappers
    if isWrapped(config)
        if func in config.wrapFunctions
            return wrapFunction(ctx, func, args...)
        else
            return wrappedRecurse(ctx, func, args...)
        end
    else
        return Cassette.recurse(ctx, func, args...)
    end
end

"""Called on a function that needs wrapping.
We wrap all arguments according to enabled wrappers and then we execute it."""
function wrapFunction(ctx::CojacCtx, func::Function, args...)
    config::WrappersConfig = ctx.metadata.config.wrappers
    if config.bigFloat.enabled
        args = wrap(ctx, BigFloatWrapper, args)
    end
    if config.bigInt.enabled
        args = wrap(ctx, BigIntWrapper, args)
    end
    if config.blank
        args = wrap(ctx, BlankWrapper, args)
    end
    func(args...)
end

"""Called on a function that don't need wrapping.
We verify if the function is applicable and unwrap the arguments if we need to."""
function wrappedRecurse(ctx::CojacCtx, func::Function, args...)
    config = ctx.metadata.config.wrappers
    if parentmodule(func) in config.ignoredModules
        return func(args...)
    end
    isApplicable = applicable(func, args...)
    # In strict mode, we compare the would-be-called method
    # with and without unwrapping to see if they are the same
    if isApplicable && config.strictMode
        unwrappedArgs = lossyUnwrap(args)
        wrappedMethod = which(func, Base.typesof(args...))
        unwrappedMethod = which(func, Base.typesof(unwrappedArgs...))
        if wrappedMethod != unwrappedMethod
            react(ctx.metadata, "Lossy strict unwrap: $(parentmodule(func)).$func")
            args = unwrappedArgs
        end
    # if not method is found, we try unwrapping the arguments
    elseif !isApplicable
        unwrapped = unwrap(args)
        # if still no method, we try a "lossy" unwrap
        if !applicable(func, unwrapped...)
            args = lossyUnwrap(args)
            if config.checkUnwraps
                react(ctx.metadata, "Lossy unwrap: $(parentmodule(func)).$func")
            end
        else
            args = unwrapped
            if config.checkUnwraps
                react(ctx.metadata, "Lossless unwrap: $(parentmodule(func)).$func")
            end
        end
    end
    # Finally, we call the method with the maybe-unwrapped args
    return Cassette.recurse(ctx, func, args...)
end