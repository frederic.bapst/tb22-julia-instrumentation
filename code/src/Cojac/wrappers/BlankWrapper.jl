struct BlankWrapper{T<:Real} <:Real
    value::T
end

"""Need to avoid multiple layers on wrappers when we pass a wrapper to the constructor"""
function BlankWrapper{T}(n::BlankWrapper{S}) where {T<:Real,S<:Real}
    return BlankWrapper{T}(n.value)
end

"""Use when we print the object with println"""
Base.show(io::IO, x::BlankWrapper) = Base.show(io, x.value)

"""Handle promotion"""
Base.promote_rule(::Type{BlankWrapper{T}}, other::Type{S}) where {T,S<:Real} = BlankWrapper{promote_type(T, other)}
Base.promote_rule(::Type{BlankWrapper{T}},
    ::Type{BlankWrapper{S}}) where {T,S<:Real}= BlankWrapper{promote_type(T, S)}

"""Workaround for matrix multiplication that still use promote_op"""
Base.promote_op(::Function, ::Type{BlankWrapper{T}}, 
    ::Type{BlankWrapper{S}}) where {T,S} = BlankWrapper{promote_type(T, S)}

"""Handle conversion from/to BlankWrapper"""
(::Type{T})(n::BlankWrapper) where T<:Real = begin
    if BlankWrapper <: T
        return n
    else
        return T(n.value)
    end
end
Base.convert(::Type{T}, n::BlankWrapper) where T<:Real = T(n)

function wrap(::CojacCtx, ::Type{BlankWrapper}, value::Real)
    return BlankWrapper{lossyUnwrap(typeof(value))}(value)
end

function wrap(::CojacCtx, ::Type{BlankWrapper}, value::Type{T}) where {T<:Real}
    return BlankWrapper{lossyUnwrap(value)}
end

"""Unwrap the BlankWrapper{T} into type T"""
unwrap(t::Type{BlankWrapper{T}}) where T<:Real = lossyUnwrap(t)
lossyUnwrap(::Type{BlankWrapper{T}}) where T<:Real = T

for op in [:+,:-,:*,:÷,:^,:%,:&,:|,:⊻,:⊼,:⊽,:>>>,:>>,:<<]
    eval(quote
        Base.$op(a::BlankWrapper, b::BlankWrapper) = begin
            res = $op(unwrap(a), unwrap(b))
            return BlankWrapper(res)
        end
    end)
end

for op in [:+,:-,:~]
    eval(quote
        Base.$op(a::BlankWrapper) = begin
            return BlankWrapper(unwrap(a))
        end
    end)
end

for op in [:/,:\,:<,:>,:<=,:>=,:!=,:(==)]
    eval(quote
        Base.$op(a::BlankWrapper, b::BlankWrapper) = begin
            return $op(unwrap(a), unwrap(b))
        end
    end)
end

"""hvcat and typed_hvcat explicitely need Int64 as their first argument"""
Base.:hvcat(rows::Tuple{BlankWrapper,BlankWrapper}, values...) = hvcat(Tuple{Int64,Int64}(rows), values...)
Base.:typed_hvcat(type::DataType, rows::Tuple{BlankWrapper,BlankWrapper}, values...) =
    Base.typed_hvcat(type, Tuple{Int64,Int64}(rows), values...)