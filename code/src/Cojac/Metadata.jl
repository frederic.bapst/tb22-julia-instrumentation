Base.@kwdef mutable struct CheckConfig
    enabled::Bool = true
    cancellationThreshold::Integer = 32
    comparisonThreshold::Integer = 32
    ignoredFunctions::Vector{Function} = [println, sleep]
end

Base.@kwdef mutable struct ReactionGraphConfig
    enabled::Bool = false
    file::String = "anomalies.png"
    size::Tuple{Integer,Integer} = (1000, 1000)
end

Base.@kwdef mutable struct ReactionConfig
    log::Bool = true
    report::Bool = false
    graph::ReactionGraphConfig = ReactionGraphConfig()
end

Base.@kwdef mutable struct BigFloatWrapperConfig
    enabled::Bool = false
    precision::Int = precision(BigFloat)
end

Base.@kwdef mutable struct BigIntWrapperConfig
    enabled::Bool = false
    bitwiseUnwrap::Bool = true
end

Base.@kwdef mutable struct WrappersConfig
    bigFloat::BigFloatWrapperConfig = BigFloatWrapperConfig()
    bigInt::BigIntWrapperConfig = BigIntWrapperConfig()
    blank::Bool = false
    strictMode::Bool = false
    checkUnwraps::Bool = true
    ignoredModules::Vector{Module} = [Core, Base]
    wrapFunctions::Vector{Function} = [+,-,*,/,÷,\,^,%,~,&,|,⊻,⊼,⊽,>>>,>>,<<,Base.literal_pow,getindex,Base.vect,
                                        Base.typed_hvcat,Base.typed_hcat,Base.typed_vcat,
                                        Base.hvcat,Base.hcat,Base.vcat]
end

Base.@kwdef mutable struct Config
    check::CheckConfig = CheckConfig()
    reaction::ReactionConfig = ReactionConfig()
    wrappers::WrappersConfig = WrappersConfig()
end

Base.@kwdef mutable struct InnerMetadata
    reactions::Dict{Tuple{String,Vector{StackTraces.StackFrame}},Integer} = Dict()
    tasks::Vector{Task} = []
end

Base.@kwdef struct Metadata
    config::Config = Config()
    inner::InnerMetadata = InnerMetadata()
end