module Cojac

using Cassette
@specialize
Cassette.@context CojacCtx

include("Cojac/Types.jl")
include("Cojac/Metadata.jl")

include("Cojac/posthooks/Posthooks.jl")
include("Cojac/wrappers/Wrappers.jl")
include("Cojac/utils/Reactions.jl")

export @cojac, Config, unwrap, lossyUnwrap

"""Ignore unwanted modules and functions or wrap instruction"""
Cassette.overdub(ctx::CojacCtx, func::Function, args...) = begin
    config = ctx.metadata.config
    if func in config.check.ignoredFunctions
        ctx = Cassette.disablehooks(ctx)
    end
    return wrappers(ctx, func, args...)
end

"""Workaround for a bug that occured on functions called with empty tuples"""
Cassette.overdub(ctx::CojacCtx, func::Function, args::Union{NamedTuple{(), Tuple{}},Tuple{}}...) = begin
    if length(args) == 0
        return wrappers(ctx, func, args...)
    end
    return func(args...)
end

"""Builtin functions cannot have multiple implementations (the "where" function throws an error with them for instance).
So they need a special case to prevent errors. Also, they are fine with every type of arguments.
(More info: https://docs.julialang.org/en/v1/devdocs/functions/#Builtins).
Must be inlined because of llvmcall (https://github.com/JuliaLabs/Cassette.jl/issues/138#issuecomment-511159993)""" 
@inline Cassette.overdub(::CojacCtx, func::Core.Builtin, args...) = func(args...)

"""
Workaround to make it works with asynchronous programming.
Taken directly from https://github.com/JuliaLabs/Cassette.jl/issues/120#issuecomment-483824502
"""
Cassette.overdub(ctx::CojacCtx{Metadata}, ::typeof(Base.Core._Task), @nospecialize(f), stack::Int, future) = begin
    r = Base.Core._Task(()->Cassette.overdub(ctx, f), stack, future)
    return r
end

"Set the post hook that will run after each instruction"
Cassette.posthook(ctx::CojacCtx, res, func, args...) = posthook(ctx.metadata, res, func, args...)

"A macro to use cojac on an expression with a config object"
macro cojac(config, expr)
    return quote
        # We initialize the metadata with the configuration
        meta = Metadata(config=$(esc(config)))
        # Launch the overdub
        res = Cassette.@overdub CojacCtx(metadata=meta) $(esc(expr))
        # Wait for all the tasks to finish
        for t in meta.inner.tasks
            if istaskstarted(t) && !istaskdone(t)
                wait(t)
            end
        end
        # And then create the graph/report if needed
        if meta.config.reaction.graph.enabled
            reactGraph(meta)
        end
        if meta.config.reaction.report
            reactReport(meta)
        end
        # Finally we return the result of the overdubbed function
        res
    end
end

"A macro to use cojac on an expression with default config"
macro cojac(expr)
    # Call the other @cojac macro with a default config object
    return :(@cojac(Config(), $(esc(expr))))
end

end