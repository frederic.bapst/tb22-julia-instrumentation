#!/usr/bin/env sh

functions=(barebone withNothing withCheck withBlankWrapper withBigFloatWrapper withBigIntWrapper withEverything)

for f in "${functions[@]}"; do
    echo -e "\n----- $f -----"
    julia -L demo/perfs.jl -e "Perfs.$f()"
    echo -e "--------------\n"
done