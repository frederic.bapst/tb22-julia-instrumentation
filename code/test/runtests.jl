module RunTests

using Test
using Cojac

using Cassette

@testset verbose=true "Cojac" begin
    include("checkTest/CheckTest.jl")
    include("wrappersTest/WrappersTests.jl")
end

end