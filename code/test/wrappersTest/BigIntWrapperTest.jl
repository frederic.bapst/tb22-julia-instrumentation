@testset "BigInt" begin

    config = Config()
    config.check.enabled = false
    config.wrappers.bigInt.enabled = true

    @testset "arithmetic operators" begin
        for T in [Int8,Int128,UInt8,UInt128]
            n = T(typemax(Int8))
            wrapped = Cojac.BigIntWrapper{T}(n)
            big = BigInt(n)
            for op in [+,-,*,÷,^,%]
                r = @cojac config op(n, n)
                @test typeof(r) == Cojac.BigIntWrapper{T}
                @test r.value == op(big, big)
                r = @cojac config op(wrapped, n)
                @test typeof(r) == Cojac.BigIntWrapper{T}
                @test r.value == op(wrapped.value, big)
            end
            # divisions must return Float64 and not Integers
            for op in [/,\]
                r = @cojac config op(n, n)
                @test typeof(r) == BigFloat
                @test r == op(big, big)
                r = @cojac config op(wrapped, n)
                @test typeof(r) == BigFloat
                @test r == op(wrapped.value, big)
            end
        end
    end

    @testset "unary operators" begin
        n = typemax(Int64)
        bigN = BigInt(n)
        wrapped = Cojac.BigIntWrapper{Int64}(n)
        for op in [+,-]
            r = @cojac config op(n)
            @test typeof(r) == Cojac.BigIntWrapper{Int64}
            @test r.value == op(bigN)
            r = @cojac config op(wrapped)
            @test typeof(r) == Cojac.BigIntWrapper{Int64}
            @test r.value == op(bigN)
        end
    end

    @testset "comparison operators" begin
        wrapped = Cojac.BigIntWrapper{Int64}(5)
        n = 6
        for op in [==,!=,<,<=,>,>=]
            r = @cojac config op(wrapped, n)
            @test typeof(r) == Bool
            @test r == op(wrapped.value, n)
        end
    end

    @testset "bitwise operators" begin
        a = Int8(100) + Int8(100)
        b = Int8(3)

        @testset "without bitwise unwrap" begin
            config.wrappers.bigInt.bitwiseUnwrap = false
            wrapped = Cojac.BigIntWrapper{Int8}(a, false)
            for op in [&,|,⊻,⊼,⊽,>>>,>>,<<]
                r = @cojac config op(wrapped, b)
                @test typeof(r) == Cojac.BigIntWrapper{Int8}
                @test r.value == op(wrapped.value, b)
            end
            config.wrappers.bigInt.bitwiseUnwrap = true
        end

        @testset "with bitwise unwrap" begin
            wrapped = Cojac.BigIntWrapper{Int8}(a, true)
            for op in [&,|,⊻,⊼,⊽,>>>,>>,<<]
                r = @cojac config op(wrapped, b)
                @test typeof(r) == Cojac.BigIntWrapper{Int8}
                @test r.value == op(a, b)
            end
        end

    end

    @testset "complex" begin
        a = typemax(Int64)
        bigA = BigInt(typemax(Int64))
        r = @cojac config a*im + a*im
        @test typeof(r) == Complex{Cojac.BigIntWrapper{Int64}}
        @test Cojac.unwrap(r) == bigA*im + bigA*im
    end

    @testset "matrix" begin
        n = typemax(Int64)
        big = BigInt(n)
        wrapped = Cojac.BigIntWrapper{Int64}(n)
        r = @cojac config [n wrapped; n n] * [n n; wrapped wrapped]
        @test typeof(r) == Matrix{Cojac.BigIntWrapper{Int64}}
        @test Cojac.unwrap(r) == [big big; big big] ^ 2
    end
    
    @testset "functions call and structs" begin
        n = Cojac.BigIntWrapper{Int64}(5)
        struct BigIntA
            val::Integer
        end
        struct BigIntB
            val::Int64
        end
        struct BigIntC{T}
            val::T
        end
        
        @testset "General function" begin
            f(::Number) = true
            @test (@cojac config f(n))
        end

        @testset "Too specific function" begin
            f(::Int64) = true
            @test (@test_warn "Lossy unwrap" @cojac config f(n))
        end
        @testset "Too specific function with lossless unwrap" begin
            f(::BigInt) = true
            @test (@test_warn "Lossless unwrap" @cojac config f(n))
        end

        @testset "Both function without strict mode" begin
            f(::Number) = true
            f(::Int64) = false
            @test (@cojac config f(n))
        end

        @testset "Both function with strict mode" begin
            f(::Number) = false
            f(::Int64) = true
            config.wrappers.strictMode = true
            @test (@test_warn "Lossy strict unwrap" @cojac config f(n))
            config.wrappers.strictMode = false
        end

        @testset "Too specific function with array" begin
            f(::Array{Int64}) = true
            @test (@test_warn "Lossy unwrap" @cojac config f([n]))
        end

        @testset "Simple generic struct" begin
            f(::BigIntA) = true
            @test (@cojac config f(BigIntA(n)))
        end

        @testset "Too specific struct" begin
            f(::BigIntB) = true
            @test (@cojac config f(BigIntB(n)))
        end

        @testset "Too specific struct with array" begin
            f(::Array{BigIntA}) = true
            @test (@cojac config f([BigIntA(n)]))
        end

        @testset "Simple parameterized struct" begin
            f(n::BigIntC{Integer}) = typeof(n.val)
            @test (@cojac config f(BigIntC{Integer}(n))) == Cojac.BigIntWrapper{Int64}
        end

        @testset "Too specific parameterized struct" begin
            f(::BigIntC{Int64}) =  true
            @test (@cojac config f(BigIntC{Int64}(n)))
        end

        @testset "Too specific local variable" begin
            f() = begin
                a::Int64 = n+1
                return true
            end
            @test (@cojac config f())
        end

        @testset "Generic local array" begin
            f() = begin
                a = [3, 4, 5]
                a[1] = n
                a[2] = 3
                a[3] = 4
                push!(a, n+1)
                push!(a, 3)
                push!(a, 4)
                return typeof(a)
            end
            @test (@cojac config f()) == Vector{Cojac.BigIntWrapper{Int64}}
        end

        @testset "Too specific local array" begin
            f() = begin
                a = Int32[3, 4, 5]
                a[1] = n
                a[2] = 3
                a[3] = 4
                push!(a, n+1)
                push!(a, 3)
                push!(a, 4)
                return typeof(a)
            end
            @test (@cojac config f()) == Vector{Cojac.BigIntWrapper{Int32}}
        end

        @testset "Generic local matrix" begin
            f() = begin
                a = [3 4; 5 2]
                return typeof(a)
            end
            @test (@cojac config f()) == Matrix{Cojac.BigIntWrapper{Int64}}
        end

        @testset "Too specific local matrix" begin
            f() = begin
                a = Int32[3 4; 5 2]
                return typeof(a)
            end
            @test (@cojac config f()) == Matrix{Cojac.BigIntWrapper{Int32}}
        end
    end

end