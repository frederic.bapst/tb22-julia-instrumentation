@testset "BigFloat" begin

    config = Config()
    config.check.enabled = false
    config.wrappers.bigFloat.enabled = true

    @testset "arithmetic operators" begin
        for T in [Float16,Float32,Float64]
            n = T(floatmax(Float16))
            wrapped = Cojac.BigFloatWrapper{T}(n)
            big = BigFloat(n)
            for op in [+,-,*,/,÷,\,^,%]
                r = @cojac config op(n, n)
                @test typeof(r) == Cojac.BigFloatWrapper{T}
                @test r.value == op(big, big)
                r = @cojac config op(wrapped, n)
                @test typeof(r) == Cojac.BigFloatWrapper{T}
                @test r.value == op(wrapped.value, big)
            end
        end
    end

    @testset "arithmetic operators with integers" begin
        wrapped = Cojac.BigFloatWrapper{Float64}(floatmax(Float64))
        for T in [Int8,Int128,UInt8,UInt128]
            n = typemax(T)
            for op in [+,-,*,/,÷,\,^,%]
                r = @cojac config op(wrapped, n)
                @test typeof(r) == Cojac.BigFloatWrapper{Float64}
                @test r.value == op(wrapped.value, n)
            end
        end
    end

    @testset "unary operators" begin
        n = floatmax(Float64)
        bigN = BigFloat(n)
        wrapped = Cojac.BigFloatWrapper{Float64}(n)
        for op in [+,-]
            r = @cojac config op(n)
            @test typeof(r) == Cojac.BigFloatWrapper{Float64}
            @test r.value == op(bigN)
            r = @cojac config op(wrapped)
            @test typeof(r) == Cojac.BigFloatWrapper{Float64}
            @test r.value == op(bigN)
        end
    end

    @testset "comparison operators" begin
        wrapped = Cojac.BigFloatWrapper{Float64}(5.2)
        n = 6.7
        for op in [==,!=,<,<=,>,>=]
            r = @cojac config op(wrapped, n)
            @test typeof(r) == Bool
            @test r == op(wrapped.value, n)
        end
    end

    @testset "complex" begin
        a = floatmax(Float64)
        bigA = BigFloat(floatmax(Float64))
        r = @cojac config a*im + a*im
        @test typeof(r) == Complex{Cojac.BigFloatWrapper{Float64}}
        @test Cojac.unwrap(r) == bigA*im + bigA*im
    end

    @testset "matrix" begin
        n = floatmax(Float64)
        big = BigFloat(n)
        wrapped = Cojac.BigFloatWrapper{Float64}(n)
        r = @cojac config [n wrapped; n n] * [n n; wrapped wrapped]
        @test typeof(r) == Matrix{Cojac.BigFloatWrapper{Float64}}
        @test Cojac.unwrap(r) == [big big; big big] ^ 2
    end
    
    @testset "functions call and structs" begin
        n = Cojac.BigFloatWrapper{Float64}(5)
        struct BigFloatA
            val::AbstractFloat
        end
        struct BigFloatB
            val::Float64
        end
        struct BigFloatC{T}
            val::T
        end
        
        @testset "General function" begin
            f(::Number) = true
            @test (@cojac config f(n))
        end

        @testset "Too specific function" begin
            f(::Float64) = true
            @test (@test_warn "Lossy unwrap" @cojac config f(n))
        end

        @testset "Too specific function with lossless unwrap" begin
            f(::BigFloat) = true
            @test (@test_warn "Lossless unwrap" @cojac config f(n))
        end

        @testset "Both function without strict mode" begin
            f(::Number) = true
            f(::Float64) = false
            @test (@cojac config f(n))
        end

        @testset "Both function with strict mode" begin
            f(::Number) = false
            f(::Float64) = true
            config.wrappers.strictMode = true
            @test (@test_warn "Lossy strict unwrap" @cojac config f(n))
            config.wrappers.strictMode = false
        end

        @testset "Too specific function with array" begin
            f(::Array{Float64}) = true
            @test (@test_warn "Lossy unwrap" @cojac config f([n]))
        end

        @testset "Simple generic struct" begin
            f(::BigFloatA) = true
            @test (@cojac config f(BigFloatA(n)))
        end

        @testset "Too specific struct" begin
            f(::BigFloatB) = true
            @test (@cojac config f(BigFloatB(n)))
        end

        @testset "Too specific struct with array" begin
            f(::Array{BigFloatA}) = true
            @test (@cojac config f([BigFloatA(n)]))
        end

        @testset "Simple parameterized struct" begin
            f(n::BigFloatC{AbstractFloat}) = typeof(n.val)
            @test (@cojac config f(BigFloatC{AbstractFloat}(n))) == Cojac.BigFloatWrapper{Float64}
        end

        @testset "Too specific parameterized struct" begin
            f(::BigFloatC{Float64}) =  true
            @test (@cojac config f(BigFloatC{Float64}(n)))
        end

        @testset "Too specific local variable" begin
            f() = begin
                a::Float64 = n+1
                return true
            end
            @test (@cojac config f())
        end

        @testset "Generic local array" begin
            f() = begin
                a = [3.2, 4, 5]
                a[1] = n
                a[2] = 3.4
                a[3] = 4
                push!(a, n+1)
                push!(a, 3.4)
                push!(a, 4)
                return typeof(a)
            end
            @test (@cojac config f()) == Vector{Cojac.BigFloatWrapper{Float64}}
        end

        @testset "Too specific local array" begin
            f() = begin
                a = Float32[3.2, 4, 5]
                a[1] = n
                a[2] = 3.4
                a[3] = 4
                push!(a, n+1)
                push!(a, 3.4)
                push!(a, 4)
                return typeof(a)
            end
            @test (@cojac config f()) == Vector{Cojac.BigFloatWrapper{Float32}}
        end

        @testset "Generic local matrix" begin
            f() = begin
                a = [3.2 4; 5 2]
                return typeof(a)
            end
            @test (@cojac config f()) == Matrix{Cojac.BigFloatWrapper{Float64}}
        end

        @testset "Too specific local matrix" begin
            f() = begin
                a = Float32[3.2 4; 5 2]
                return typeof(a)
            end
            @test (@cojac config f()) == Matrix{Cojac.BigFloatWrapper{Float32}}
        end
    end

end