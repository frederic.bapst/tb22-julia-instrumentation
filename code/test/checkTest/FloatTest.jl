@testset "floats" begin

    for T in [Float16,Float32,Float64,BigFloat]
        big = floatmax(T) / 2
        big2 = big + eps(big)
        small = floatmin(T) * 2
        inf = T(Inf)
        
        @testset "absorption $T" begin
            @test_warn "Absorption: FADD" @cojac big + small
            @test_warn "Absorption: FADD" @cojac small + big
            @test_warn "Absorption: FSUB" @cojac big - small
            @test_warn "Absorption: FSUB" @cojac small - big
            @test_warn "Absorption: FREM" @cojac big % small

            @test_nowarn @cojac inf + small
            @test_nowarn @cojac small + inf
            @test_nowarn @cojac inf - small
            @test_nowarn @cojac small - inf
            @test_nowarn @cojac inf % small
        end
        
        @testset "cancellation $T" begin
            @test_warn "Cancellation: FADD" @cojac big + (-big2)
            @test_warn "Cancellation: FADD" @cojac (-big) + big2
            @test_warn "Cancellation: FSUB" @cojac big - big2
            @test_warn "Cancellation: FSUB" @cojac (-big) - (-big2)
        end
        
        @testset "comparison $T" begin
            @test_warn "Comparing very close: DCMP" @cojac big < big2
            @test_warn "Comparing very close: DCMP" @cojac big <= big2
            @test_warn "Comparing very close: DCMP" @cojac big2 > big
            @test_warn "Comparing very close: DCMP" @cojac big2 >= big

            @test_nowarn @cojac small < big
            @test_nowarn @cojac small <= big
            @test_nowarn @cojac big > small
            @test_nowarn @cojac big >= small
        end

        @testset "underflow $T" begin
            @test_warn "Underflow: DDIV" @cojac small / big
            @test_warn "Underflow: DDIV" @cojac big \ small
            @test_warn "Underflow: DMUL" @cojac small * small
            @test_warn "Underflow: DPOW" @cojac small ^ big
        end

        @testset "Inf and NaN $T" begin
            @test_warn "Inf float operation: +" @cojac big + 2*big
            @test_warn "-Inf float operation: -" @cojac -big - 2*big
            @test_warn "NaN float operation: /" @cojac 0/0
        end


    end

end
