@testset "integers overflow" begin

    for T in [Int8,Int128,UInt8,UInt128]
        min = typemin(T)
        max = typemax(T)
        two = T(2)
        one = T(1)
    
        @testset "$T" begin
            @test_warn "Overflow : IADD" @cojac max + two
            @test_warn "Overflow : INEG" @cojac -min
            @test_warn "Overflow : ISUB" @cojac min - two
            @test_warn "Overflow : IMUL" @cojac max * two

            @test_nowarn @cojac -two
            @test_nowarn @cojac max - two
            @test_nowarn @cojac two * two
            @test_nowarn @cojac min + two
        end
    
        @testset "complex $T" begin
            @test_warn "Overflow : IADD" @cojac max*im + two*im
            @test_warn "Overflow : ISUB" @cojac min*im - two*im
            @test_warn "Overflow : IMUL" @cojac max*im * two*im
            @test_warn "Overflow : IMUL" @cojac max*im * two

            if two isa Unsigned
                @test_warn "Overflow : ISUB" @cojac two*im * two*im
            else
                @test_nowarn @cojac two*im * two*im
            end

            @test_nowarn @cojac two*im + two*im
            @test_nowarn @cojac two*im - two*im
            @test_nowarn @cojac two * two*im
        end
    
        @testset "matrix $T" begin
            @test_warn "Overflow : IADD" @cojac [max one] + [two one]
            @test_warn "Overflow : IADD" @cojac [max two; one one] * [one one; one one]
            @test_warn "Overflow : IMUL" @cojac [max one; one one] * [two one; one one]
            @test_warn "Overflow : IMUL" @cojac [max one; one one] ^ two

            @test_nowarn @cojac [two two; two two] + [two two; two two]
            @test_nowarn @cojac [two two; two two] * [two two; two two]
        end
    end

end