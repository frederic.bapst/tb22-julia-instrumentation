module WrappersDemo

using Cojac

function run()
    config = Config()
    config.wrappers.bigFloat.enabled = true
    config.wrappers.bigFloat.precision = 300
    config.wrappers.bigInt.enabled = true
    println("Without Wrappers :")
    demo()
    println("------------------")
    println("With Wrappers :")
    @cojac config demo()
    println("------------------")
end

function demo()
    println("pi = $(2*pi/2)")
    println("10^20 = ", 10^20)
    println("muller(50) = $(mullerRecurrence(50))")
end

function mullerRecurrence(n)
    values = 4, 4.25
    for _=1:n
        values = values[2], muller(values[1], values[2])
    end
    return values[1]
end

function muller(a, b)
    return 108 - (815-1500/a)/b
end

end