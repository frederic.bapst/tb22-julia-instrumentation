module Perfs
using Cojac

include("demo.jl")

function perfs(config)
    println("with compilation")
    @time @cojac config run(10)
    println("without compilation")
    @time @cojac config run(10)
    println("without compilation x10")
    @time @cojac config run(100)
end

function barebone()
    println("with compilation")
    @time run(10)
    println("without compilation")
    @time run(10)
    println("without compilation x10")
    @time run(100)
end

function run(n)
    for _=1:n
        Demo.run()
    end
end

function withNothing()
    config = Config()
    config.check.enabled = false
    perfs(config)
end

function withCheck()
    config = Config()
    config.reaction.log = false
    perfs(config)
end

function withBlankWrapper()
    config = Config()
    config.check.enabled = false
    config.wrappers.blank = true
    perfs(config)
end

function withBigFloatWrapper()
    config = Config()
    config.check.enabled = false
    config.wrappers.bigFloat.enabled = true
    perfs(config)
end

function withBigIntWrapper()
    config = Config()
    config.check.enabled = false
    config.wrappers.bigInt.enabled = true
    perfs(config)
end

function withEverything()
    config = Config()
    config.reaction.log = false
    config.wrappers.bigFloat.enabled = true
    config.wrappers.bigInt.enabled = true
    perfs(config)
end

end