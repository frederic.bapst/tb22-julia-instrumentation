module Demo

function run()
    for i=1:100
        for i=1:3
            cancellation()
            comparison()
            underflow()
        end
        for i=1:5
            overflow(i)
            absorption()
        end
    end
end

function runThreads()
    Threads.@threads for i=1:100
        @async for i=1:3
            cancellation()
            comparison()
            underflow()
        end
        for i=1:5
            overflow(i)
            absorption()
        end
    end
end

function overflow(i::Integer)
    a = 2^62
    b = 10
    r = (i%2 == 0) ? a*b : a+a
end

function absorption()
    a = 1e50
    b = 1e-50
    r = a + b
end

function cancellation()
    a = 1e10
    b = a + 1e-5
    r = a - b
    r = a - b
end

function comparison()
    a = 1e10
    b = a + 1e-5
    r = a < b
end

function underflow()
    a = 1e-200
    b = 1e200
    r = a / b
end

end