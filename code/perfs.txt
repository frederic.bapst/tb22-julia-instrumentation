
----- barebone -----
with compilation
  0.000035 seconds
without compilation
  0.000035 seconds
without compilation x10
  0.000345 seconds
--------------


----- withNothing -----
with compilation
  4.087485 seconds (16.18 M allocations: 724.558 MiB, 8.06% gc time, 65.33% compilation time)
without compilation
  1.390297 seconds (7.15 M allocations: 228.288 MiB, 1.82% gc time)
without compilation x10
 14.067659 seconds (71.45 M allocations: 2.229 GiB, 1.85% gc time)
--------------


----- withCheck -----
with compilation
 13.827990 seconds (27.12 M allocations: 1.409 GiB, 2.73% gc time, 20.40% compilation time)
without compilation
 11.036399 seconds (17.94 M allocations: 938.160 MiB, 0.93% gc time)
without compilation x10
112.699366 seconds (179.39 M allocations: 9.162 GiB, 0.89% gc time)
--------------


----- withBlankWrapper -----
with compilation
  1.839245 seconds (5.58 M allocations: 278.729 MiB, 12.36% gc time, 89.32% compilation time)
without compilation
  0.184947 seconds (1.05 M allocations: 26.177 MiB)
without compilation x10
  1.846145 seconds (10.47 M allocations: 261.754 MiB, 1.47% gc time)
--------------


----- withBigFloatWrapper -----
with compilation
  1.788594 seconds (5.42 M allocations: 273.118 MiB, 5.55% gc time, 87.31% compilation time)
without compilation
  0.207901 seconds (1.13 M allocations: 32.128 MiB)
without compilation x10
  2.088413 seconds (11.32 M allocations: 321.263 MiB, 2.64% gc time)
--------------


----- withBigIntWrapper -----
with compilation
  1.855285 seconds (5.83 M allocations: 290.595 MiB, 12.17% gc time, 87.12% compilation time)
without compilation
  0.242770 seconds (1.16 M allocations: 30.220 MiB, 5.67% gc time)
without compilation x10
  2.240308 seconds (11.59 M allocations: 302.190 MiB, 3.28% gc time)
--------------


----- withEverything -----
with compilation
  8.720484 seconds (12.22 M allocations: 753.447 MiB, 3.43% gc time, 24.42% compilation time)
without compilation
  6.452434 seconds (6.89 M allocations: 455.094 MiB, 0.84% gc time)
without compilation x10
 64.484805 seconds (68.93 M allocations: 4.444 GiB, 0.87% gc time)
--------------

