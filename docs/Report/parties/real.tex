Ce chapitre décrit les spécificités de l'implémentation de l'outil ainsi que les problèmes rencontrés durant celle-ci
avec les solutions qui y ont été apportées.

\section{Objet "Metadata"}
Le contexte de Cassette permet d'y intégrer des métadonnées du type que l'on veut.
Pour ce projet, ces métadonnées seront sous forme d'un objet "Metadata" contenant d'une part la configuration
de Cojac, et d'autres certaines informations nécessaires au fonctionnement de l'outil (comme par exemple la liste des
anomalies détectées afin de pouvoir créer un rapport final).

\section{Détection d'anomalies}
La détection d'anomalie se fait à l'aide d'un posthook Cassette, on doit ensuite potentiellement traiter les anomalies
détectées pour créer afficher rapport à la fin de l'exécution de notre programme, ceci se fait à l'intérieur
même de notre macro \inline{@cojac}, après l'instrumentation.

\subsection{Repérage des anomalies}
En utilisant le multiple-dispatch, on peut faire en sorte que le hook ne se déclenche que lorsque les opérateurs
problématiques sont appelés. Ayant accès aux opérandes ainsi qu'au résultat de l'opération, on peut ainsi détecter
s'il y a une anomalie. La figure \ref{fig:detected} montre un exemple de l'implémentation de la détection d'absorptions
sur les additions et les soustractions.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
        checkAbsorption(args...) = nothing

        function checkAbsorption(meta::Metadata, ::typeof(+), r::AbstractFloat, a::Number, b::Number)::Nothing
            # {...}
        end

        function checkAbsorption(meta::Metadata, ::typeof(-), r::AbstractFloat, a::Number, b::Number)::Nothing
            # {...}
        end
        \end{juliacode}
      \caption{Partie de l'implémentation de la détection d'absorptions}
      \label{fig:detected}
    \end{center}
\end{figure}

Pour pouvoir utiliser la pleine puissance du multiple dispatch, il est nécessaire d'avoir une définition
générale de notre fonction qui va être appelée pour toutes les types d'arguments ne correspondant pas à ce que l'on
cherche. C'est à ça que sert la première ligne de la figure \ref{fig:detected}. Ainsi, tout appel à 
\inline{checkAbsorption} n'ayant pas les bons types d'arguments sera un no-op, ceci permet de ne pas avoir à 
checker les types dans un immense "if statement".

\subsubsection{Fonctions ignorées}
L'implémentation de certaines fonctions (comme println) génère des anomalies de manière contrôlée. Il n'est donc
pas nécessaire de les repérer. C'est pourquoi une liste de fonction à ignorer est ajoutée dans la configuration. 

\subsection{Réaction aux anomalies}
Une fois détectée, l'anomalie est reportée à l'aide d'une fonction "react". Celle-ci prend en argument l'objet
"Metadata" ainsi qu'une chaîne de caractères décrivant l'anomalie. Celle-ci est ensuite traitée selon la configuration
choisie.

\subsubsection{logging}
Pour le logging de l'anomalie on souhaite, en plus de sa description, obtenir la ligne à laquelle celle-ci a été
détectée Pour cela, on récupère la stacktrace actuelle et on essaie de trouver la stackframe qui correspond le plus
à ce qui peut être utile à l'utilisateur.

Pour cela, il remonte la stacktrace jusqu'à tomber sur une frame trouvée "intéressante".
Une frame est jugée intéressante si le fichier auquel elle se rapporte ne contient pas certains mots-clefs comme "Cojac".

\subsubsection{Rapport final}
Le rapport final permet d'obtenir, à la fin de l'instrumentation, un rapport listant toutes les anomalies qui ont été
détectées avec le nombre de fois qu'elles ont été détectées par ligne. La figure \ref{fig:finalReport} montre un exemple
d'un tel rapport.

\begin{figure}[H]
    \begin{center}
    \begin{textcode}
       COJAC Anomalies Report
    ----------------------------
    Underflow: DDIV
        300 times -> demo.jl:45
    Cancellation: FSUB
        300 times -> demo.jl:32
        300 times -> demo.jl:33
    Absorption: FADD
        500 times -> demo.jl:26
    Overflow : IADD
        300 times -> demo.jl:20
    Overflow : IMUL
        200 times -> demo.jl:20
    Comparing very close: DCMP
        300 times -> demo.jl:39
    ----------------------------
    \end{textcode}
    \caption{Exemple de rapport final d'anomalies}
    \label{fig:finalReport}
  \end{center}
\end{figure}

Chaque anomalie est ainsi regroupée par type, puis par endroit détecté, avec le nombre de fois que celle-ci a été
détectée

\subsubsection{Graphe de stacktrace}
Ce dernier type de rapport permet de créer un graphe résumant pour chaque anomalie la stacktrace l'ayant générée.
Pour éviter de polluer le graphe, seules les stackframes jugée intéressantes sont affichées

La figure \ref{fig:anomaliesGraph} donne un exemple de graphe de stacktrace.

\img{anomaliesGraph}{Exemple de graphe d'anomalies}

Ce graphe est généré en utilisant les librairies \link{Graphs.jl}{https://juliapackages.com/p/graphs} permettant
de créer la structure de graphe, et \link{Plots.jl}{https://juliapackages.com/p/plots} afin de dessiner l'image 
du graphe.

\section{Système de wrapper}
Le wrapping se fait à l'aide de la fonction overdub de Cassette. On modifie ainsi le comportement
de chaque instruction appelée par le programme pour pouvoir wrap/unwrap les arguments au besoin.

\subsection{Forme général des wrappers}
Chaque wrapper créé prend la forme d'une structure paramétrisée. Le paramètre permet de garder une trace
du type que devrait avoir la variable si elle n'avait pas été wrappée. Ceci afin de pouvoir unwrapper la variable
dans le type correct. La figure \ref{fig:wrapper} montre un exemple de wrapper simple.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            struct BlankWrapper{T<:Real} <:Real
                value::T
            end
        \end{juliacode}
      \caption{La structure du wrapper Blank, avec sa valeur et son paramètre}
      \label{fig:wrapper}
    \end{center}
\end{figure}

De plus, les wrappers sont définis comme des sous-types d'un type numérique abstrait (Integer pour le wrapper BigInt,
AbstractFloat pour le wrapper BigFloat...). Ceci leur permet d'être passés directement à des fonctions ayant des
arguments de type abstrait.

\subsection{Wrapping des arguments}
La création d'une variable n'est pas une instruction que Cassette permet d'intercepter. On ne peut donc pas
directement wrapper les variables à leur création. À la place, il existe une liste de fonctions spéciales
(les "wrapFunctions"), modifiables dans la configuration, dont les arguments seront wrappés. Par défaut cette liste
contient les opérateurs arithmétiques et bitwise, ainsi que les fonctions qui sont appelées lors de la création d'un
vecteur ou d'une matrice.

Pour wrapper les arguments, on fait appel à une fonction \inline{wrap} qui prend en argument le contexte Cassette,
le type de wrapper concerné ainsi que les arguments de la fonction à appeler. 
Chaque wrapper doit ensuite s'occuper de créer une fonction à ce nom qui retourne un wrapper à partir d'un argument.

Si aucune fonction n'a été créée pour un type d'argument en particulier, une implémentation par défaut retourne
simplement l'argument. Ainsi, tous les arguments n'ayant pas besoin d'être wrappés sont retournés tel quel.
De plus, une instance générique de la fonction \inline{wrap} est implémentée sur les types de collection
(Vecteurs, Matrices et Tuples) afin de wrapper l'ensemble de leur contenu.

La figure \ref{fig:wrap} illustre un exemple de fonction wrap sur un wrapper de nombre réel.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
        function wrap(::CojacCtx, ::Type{BlankWrapper}, value::Real)
            return BlankWrapper{lossyUnwrap(typeof(value))}(value)
        end
        function wrap(::CojacCtx, ::Type{BlankWrapper}, value::Type{T}) where {T<:Real}
            return BlankWrapper{lossyUnwrap(value)}
        end
        \end{juliacode}
      \caption{Implémentation du wrapping sur le wrapper Blank}
      \label{fig:wrap}
    \end{center}
\end{figure}

On peut voir sur cette figure que deux instances de la fonction sont créées. La première permet de wrapper
les nombres réels alors que la seconde s'occupe des objets \inline{Type}. Ceci permet de faire en sorte qu'une
fonction prenant comme argument un type reçoive le wrapper au lieu du type de base
(dans cet exemple, elle recevra \inline{BlankWrapper\{Int64\}} au lieu de Int64).
Le \inline{lossyUnwrap} dans le paramètre du constructeur permet d'éviter l'imbrication des wrappers
(\inline{BlankWrapper\{BlankWrapper\{Int64\}\}}).


\subsection{Unwrapping des arguments}
Les fonctions ne faisant pas partie des "wrapFunctions" nécessitent tout de même de passer par le système de wrapping.
En effet, bien que cela soit découragé par la documentation de Julia, il est possible et parfois nécessaire
de préciser les arguments de façon concrète.

Comme Cassette ne nous permet pas de modifier la signature d'une méthode, il nous faut dans ce cas unwrapper les
arguments afin de pouvoir appeler notre fonction. La fonction Julia \inline{applicable} permet de vérifier
si une fonction est applicable à une liste de types d'arguments donnés.

Cet unwrapping se fait en deux étapes. Chaque wrapper possède deux méthodes: \inline{unwrap} et \inline{lossyUnwrap}.
La première change le type de l'objet sans perte de précision
(par exemple: \inline{BigFloatWrapper $\rightarrow$ BigFloat}), alors que le second retourne le type qu'aurait eu le
nombre s'il n'y avait pas eu de wrapper (\inline{BigFloatWrapper $\rightarrow$ Float64}).

Ainsi, lorsqu'une fonction ne peut pas être appelée avec un wrapper, on tente d'abord d'effectuer un unwrap sans perte.
Si la fonction n'est toujours pas appelable, on effectue un lossyUnwrap. Dans les deux cas, il est possible de détecter
ces unwraps comme des anomalies puis de les rapporter comme indiqué dans la section \ref{conception:reports}.

\subsubsection*{Mode strict}
Un flag dans la configuration permet d'enclencher le mode strict. Il s'agit d'un mode qui modifie la manière
dont se fait le unwrapping. En mode strict, on vérifie pour chaque instruction à l'aide de la fonction Julia
\inline{which} quelle implémentation serait appelée si on n'avait pas utilisé de wrapper. Dans le cas où cette
implémentation n'est pas la même que celle appelée avec les wrappers, on effectue un lossyUnwrap avant d'appeler
la fonction. Dans la figure \ref{fig:strictMode}, un appel à la fonction f avec un wrapper sur les Integer retournera
un résultat différent en mode classique et en mode strict.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            function f(a::Int64)
                return "called in strict mode"
            end

            function f(a::BigInt)
                return "called in classic mode"
            end
        \end{juliacode}
      \caption{Fonction générant un chemin différent en mode classique et en mode strict}
      \label{fig:strictMode}
    \end{center}
\end{figure}

Cette fonctionnalité permet ainsi de s'assurer que l'on passe par les mêmes implémentations de fonction avec un wrapper
et sans, au prix d'une perte de précision. Cette perte peut également être détectée comme anomalie.

\subsection{Conversions et promotions}
Il sera parfois nécessaire de convertir nos wrappers en un autre type, par exemple lorsque l'on en associe la valeur
à une variable typée (par exemple: \inline{v::Float64 = monWrapper}). Il faut donc créer des règles de conversion.

Il existe deux types de conversions en Julia: la conversion explicite, qui utilise le constructeur du type cible
(par exemple: \inline{Float32(value)}), et la conversion implicite qui utilise la fonction convert 
(par exemple: \inline{convert(Float32, value)}).
Il est donc nécessaire d'implémenter ces deux fonctions pour chacun de nos wrappers.
Pour les wrappers implémentés durant ce projet, les fonctions de conversions délèguent celle-ci à la au champ "value"
de l'objet.

Quant à la promotion, il s'agit d'une opération permettant de transformer plusieurs variables de types différents
en un même type unique. Elle est par exemple utilisée lorsque l'on effectue une opération sur deux types différents
(par exemple, une addition d'un Float et d'un Int) afin de ne pas avoir à implémenter l'opérateur pour chaque
combinaison de types possible.

Cette promotion s'implémente à l'aide de la fonction \inline{promote\_rule} qui prend deux types en arguments
et retourne le type dans lequel ils doivent s'unifier. Pour nos wrappers, cela permet de définir le type résultant
d'une opération entre deux objets Wrappers dont le type d'origine est différent.
Plusieurs fonctions de Julia utilisent ensuite les règles de promotions ainsi définies, par exemple
à l'aide de \inline{promote} qui prend un Vararg en entrée et retourne un Tuple contenant les arguments unifiés en un
même type.

La figure \ref{fig:convertTypes} illustre les conversions implicites et explicites ainsi que le système de promotion.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            struct MyType <: Number
            end

            # Conversion implicite
            Base.convert(::Type{Int64}, ::MyType) = 1

            # Conversion explicite
            Int64(::MyType) = 2

            # Règle de promotion
            Base.promote_rule(::Type{MyType}, ::Type{T}) where {T<:Number} = Int64

            myVar = MyType()
            a::Int64 = myVar
            println("a = ", a)
            b = Int64(myVar)
            println("b = ", b)
            tuple = promote(3.0, myVar)
            println("tuple = ", tuple)
            c = 3.0 + myVar
            println("c = ", c)
        \end{juliacode}
        \begin{textcode}
            a = 1
            b = 2
            tuple = (3, 1)
            c = 4
        \end{textcode}
      \caption{Utilisation des conversions et promotions}
      \label{fig:convertTypes}
    \end{center}
\end{figure}

Dans cette exemple, une conversion implicite d'une structure \inline{MyStruct} vers \inline{Int64} retourne 1,
alors qu'une conversion explicite retourne 2. De plus, on définit une règle de promotion afin qu'une opération
entre une variable \inline{MyStruct} et tout autre nombre retourne une valeur de type \inline{Int64}, réalisant une
conversion implicite en passant.

\subsubsection*{Cas des matrices}
Une autre fonction de promotion, appelée \inline{promote\_op} existe également, elle prend en argument une fonction
ainsi qu'une liste de types et retourne le type devant résulter de cette fonction avec les types donnés en argument.
Cette fonction est peu documentée et semble être plus ou moins dépréciée Il a cependant été remarqué qu'elle était
encore utilisée lors de certaines opérations sur les matrices, c'est pourquoi il est tout de même nécessaire de
l'implémenter.

\subsection{Définition d'opérateurs}
Pour chacun de nos wrappers, il est nécessaire de définir les opérateurs qui s'y appliqueront. Nombre de ces opérateurs
auront une logique similaire, c'est pourquoi on peut utiliser la fonctionnalité de génération de code de Julia.

Celle-ci permet d'évaluer dynamiquement du code à l'aide de la fonction \inline{eval}. Ceci afin de pouvoir par exemple
créer plusieurs fonctions d'un coup à l'aide d'une boucle. La figure \ref{fig:codeGen} montre comment on peut définir
plusieurs opérateurs d'un seul coup pour notre wrapper BigInt.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            for op in [:+,:-,:*,:÷,:^,:%]
                eval(quote
                    Base.$op(a::BigIntWrapper, b::BigIntWrapper) = begin
                        type = Base.promote_typeof(a, b)
                        res = $op(unwrap(a), unwrap(b))
                        return type(res)
                    end
                end)
            end
        \end{juliacode}
      \caption{Définitions de six opérateurs à l'aide de la génération de code}
      \label{fig:codeGen}
    \end{center}
\end{figure}

Dans cet exemple, chaque opérateur se contente de récupérer le type cible à l'aide du système de promotion, d'effectuer
le calcul sur la valeur contenue par le wrapper puis de retourner un nouveau wrapper avec le type voulu.

\subsection{Fonctions à Tuples vides}
Il a été remarqué que certaines fonctions internes à Julia affichaient une erreur lors de l'overdub
quand les arguments de la fonction étaient composés de Tuples vides. Cette erreur ne perturbait en général pas le
cours du programme mais était tout de même gênante Après quelques recherches il apparaît qu'il s'agit d'un problème
en rapport avec des appels internes au code Julia, mais la cause exacte n'a pas pu être identifiée.

Pour régler temporairement ce souci, on gère spécifiquement les fonctions ayant pour seuls arguments des Tuples vides
afin d'éviter spécifiquement de réaliser un overdub dessus. Il s'agit néanmoins d'une solution non-idéale et ce problème
mériterait qu'on se penche plus longuement dessus.

\subsection{Fonction Builtin}
Certaines fonctions Julia sont dérivées du type \inline{Core.Builtin}. Ces fonctions ont un statut spécial puisqu'elles
n'ont pas de méthodes associées. À la place, leur seule implémentation peut prendre n'importe quel type d'argument.
Cette implémentation n'est en revanche pas retournée par la fonction \inline{which} qui est utilisée pour le mode
strict, ce qui nécessite une implémentation particulière.

De plus, certaines fonctions Builtin, comme \inline{Core.Intrinsics.llvmcall} causent un bug Cassette nécessitant
d'annoter la fonction overdub comme étant inline afin d'éviter une \link{exception}{https://github.com/JuliaLabs/Cassette.jl/issues/138}.

Pour toutes ces raisons, il a été décidé de considérer les fonctions Builtin différemment et de les appeler directement
au lieu de les overdub. Ceci ne pose pas de problème particulier car il s'agit typiquement de fonctions ne devant
pas demander de wrapping et ne générant pas d'anomalies (on y trouve par exemple les fonctions typeof, throw, sizeof, 
etc.)

\subsection{Spécificités des wrappers}
En plus des généralités citées dans les sections précédentes, les wrappers possèdent des caractéristiques spécifiques
dans leur implémentation.

\subsection{Wrapper BigFloat}
Voici les spécificités d'implémentation du wrapper BigFloat :
\subsubsection{Gestion de la précision}
En Julia, la précision d'un BigFloat est définie à deux endroits : la variable BigFloat en elle-même qui possède un 
champs définissant sa précision, et aussi lors du calcul où l'on choisit la précision de celui-ci à l'aide de la fonction
\inline{setprecision} indépendamment de la précision des opérandes.

Il est donc nécessaire dans notre wrapper de gérer ces deux définitions de la précision. La première se fait dans la
fonction \inline{wrap}. C'est à ce moment-là que l'on a accès à la précision configurée par l'utilisateur et que l'on
crée notre variable BigFloat. On en profite donc pour le créer avec la précision souhaitée.

La seconde définition est plus subtile à gérer, car au moment du calcul, nous n'avons accès qu'aux opérandes de la
fonction et en aucun cas à la configuration. Heureusement, comme on a défini la précision de nos BigFloat selon
cette configuration, on peut se baser sur celle-ci pour effectuer le calcul. La figure \ref{fig:floatOp} illustre
comment on définit cette précision pour le calcul en cours.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
        Base.$op(a::BigFloatWrapper, b::BigFloatWrapper) = begin
            type = Base.promote_typeof(a, b)
            precision = max(a.value.prec, b.value.prec)
            return setprecision(precision) do
                res = $op(unwrap(a), unwrap(b))
                return type(res, precision)
            end
        end
        \end{juliacode}
      \caption{Récupération de la précision des opérandes pour un calcul sur les wrappers BigFloat}
      \label{fig:floatOp}
    \end{center}
\end{figure}

Dans cet exemple, le mot-clef "do" permet de créer une fonction anonyme qui est passé en argument de \inline{setprecision},
il s'agit en fait d'un équivalent à la syntaxe \inline{setprecision(()->\{...\}, precision)}.

\subsubsection{Opérations arithmétiques avec un entier}
En Julia, les opérateurs arithmétiques possèdent diverses implémentations selon les types des opérandes.
En particulier, les opérations entre un BigFloat et un entier diffèrent sensiblement de leur équivalent
entre un Float "classique" et ce même entier. Par exemple, certaines puissances sont transformées en séries
de multiplications dans un cas et pas un autre, ce qui peut donner un résultat différent.

Il est donc nécessaire d'ajouter des implémentations spéciales de nos opérateurs afin que ces derniers s'effectuent 
de la même manière sur nos wrappers que sur un BigFloat au lieu d'un Float classique.

De plus, cet ajout crée un conflit avec une autre implémentation de ces opérateurs portant sur les booléens.
En effet, il y a une ambiguïté entre notre implémentation \inline{op(::BigFloatWrapper, ::Integer)} et 
l'implémentation de base \inline{op(::AbstractFloat, ::Bool)}, ce qui lève une exception lors d'un appel de la 
fonction avec un BigFloatWrapper (sous-type de AbstractFloat) et un Bool (sous-type de Integer). Ce genre de calcul
est particulièrement courant lorsque l'on travaille avec les nombres complexes puisque la constante \inline{im} est de
type booléen complexe.

Pour lever cette ambiguïté, il est donc nécessaire d'ajouter une implémentation spécifique de chaque opérateur pour
le type Bool.

\subsection{Wrapper BigInt}
Voici les spécificités d'implémentation du wrapper BigInt :

\subsubsection{Création de matrice}
Pour créer une matrice à partir d'un littéral, Julia peut faire appel à deux fonctions différentes : \inline{hvcat} si
l'on créer une matrice sans en préciser le type et \inline{typed\_hvcat} lorsque l'on précise le type de la matrice.

Ces fonctions possèdent une signature particulière: un Tuple de Int64 précise les dimensions
de la matrice alors que les arguments suivants, sous forme de Varargs, donnent son contenu.
(Pour les matrices typées, un argument supplémentaire précisant le type se place tout devant).

Le problème avec cette signature est que les dimensions de la matrice doivent être de type Int64 explicitement.
En particulier, celles-ci ne peuvent pas être des BigFloatWrapper. Ce qui fait qu'un appel à ces méthodes
générera un unwrapping des arguments, il devient donc impossible de créer une matrice de Wrapper à partir de littéraux

Pour contourner ce problème, il a fallu créer une implémentation particulière de ces deux méthodes prenant un Tuple de
Wrapper à la place des Int64 et le convertir avant d'appeler la méthode de base. la figure \ref{fig:hvcat} montre les
détails de cette implémentation.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            Base.:hvcat(rows::Tuple{BigIntWrapper,BigIntWrapper}, values...) = hvcat(Tuple{Int64,Int64}(rows), values...)
        \end{juliacode}
      \caption{Implémentation de hvcat avec notre wrapper BigInt}
      \label{fig:hvcat}
    \end{center}
\end{figure}

\subsection{Wrapper Blank}
Le wrapper Blank enveloppant toutes les valeurs numériques réelles, il est nécessaire d'implémenter les méthodes
\inline{hvcat} et \inline{types\_hvcat} de la même façon que pour le wrapper BigInt. En dehors de ça, ce wrapper
se contente d'implémenter les fonctions nécessaires au wrapping/unwrapping.

\section{Programmation concurrente}
En Julia, la programmation concurrente se fait à l'aide d'objets \inline{Task} que l'on lance de manière asynchrone
à l'aide de la fonction \inline{schedule}. Ceci est en général caché derrière des macros comme \inline{@async} qui
crée et lance une tâche exécutant l'expression passée en paramètre ou encore \inline{Threads.@threads} qui permet
de paralléliser une boucle for. Mais au final, tout passe par cet objet Task et cette fonction schedule.

\subsection{Inspection du code interne des tâche}
Par défaut, Cassette n'inspecte pas le code effectué dans une tâche. Pour contourner cet obstacle, on peut
utiliser la fonction overdub afin de modifier toutes les fonctions retournant une tâche. On modifie ensuite
le contenu de cette tâche pour y appeler manuellement la fonction overdub.

\subsection{Rapport d'anomalies et code asynchrone}
La création d'un rapport ou d'un graphe d'anomalies nécessite d'être certain que le code inspecté a fini de tourner.
Lors de l'utilisation de la programmation concurrente, il est tout à fait possible que l'overdub retourne alors que
toutes les tâches ne sont pas encore terminées.

Pour remédier à ce problème, on ajoute un posthook sur la fonction \inline{schedule}. Celui-ci remplira un tableau,
situé dans les métadonnées du contexte Cassette, contenant toutes les tâches qui ont été lancées durant l'inspection.
À la fin de l'inspection, on se contente alors d'attendre que toutes ces tâches se terminent à l'aide de la fonction
\inline{wait}. Une fois qu'elles sont toutes terminées, on peut générer les éventuels rapports.