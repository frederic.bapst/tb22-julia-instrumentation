Ce chapitre décrit l'environnement de test unitaire de Julia et liste tous les cas de test qui furent réalisés.

\section{Environnement de test}
Le module "Test" de Julia permet de réaliser des tests unitaires\cite{julia-unit-tests}. Celui-ci contient diverses macros
permettant d'écrire ces tests.

Trois de ces macros nous ont été utiles ici. La première \inline{@testset} permet de structurer nos tests
en sections ayant chacun un nom et pouvant accueillir plusieurs cas de tests.

Chaque cas de test est inclus dans une
macro \inline{@test}, il s'agit de l'objet principal de la librairie puisqu'elle évalue une expression booléenne qui
définit si le test passe ou non.

Enfin, une macro \inline{@test\_warn} permet de tester qu'une expression lance un
log de type "warning", ce qui est très utile pour tester la détection des anomalies.

La figure \ref{fig:testExample} montre un exemple de testset contenant deux tests simples.

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        function f()
            @warn "Attention !!!"
        end

        @testset "Mon test" begin
            @test 3+5 == 8
            @test_warn "Attention !!!" f()
        end
    \end{juliacode}
      \caption{Exemple de cas de test en Julia}
      \label{fig:testExample}
    \end{center}
\end{figure}

\section{Cas des test}
Voici les différents cas de tests qui ont été réalisés durant ce projet.

\subsection{Détection d'anomalies}
Ces tests sont effectués pour vérifier le bon fonctionnement de la détection d'anomalies.

\subsubsection{Sur les integers}
Ces tests vérifient les anomalies sur les types entiers.

\begin{longtable}{ | p{.30\textwidth} | p{.60\textwidth}  | c | }
    \hline
    \textbf{Nom du test} & \textbf{Description} & \textbf{Visa} \\ \hline

    Overflows sur les réels
    & Détection des overflows sur les additions, soustractions, multiplications et opposés
    sur les types Int8, Int128, UInt8 et UInt128
    & \ok \\ \hline
    
    Overflows sur les complexes
    & Détection des overflows sur les opérations sur les nombres complexes
    & \ok \\ \hline
    
    Overflows sur les matrices
    & Détection des overflows sur les opérations matricielles
    & \ok \\[.5cm] \hline

\end{longtable}


\subsubsection{Sur les floats}
Ces tests vérifient les anomalies sur les types à virgule flottante.
\begin{longtable}{ | p{.30\textwidth} | p{.60\textwidth}  | c | }
    \hline
    \textbf{Nom du test} & \textbf{Description} & \textbf{Visa} \\ \hline
    
    Absorptions sur les floats
    & Détection des absorptions sur les additions, soustractions et modulos
    & \ok \\ \hline
    
    Cancellations sur les floats
    & Détection des cancellation sur les additions et soustractions
    & \ok \\[.5cm] \hline
    
    Comparaisons discutables sur les floats
    & Détection des comparaisons discutables
    & \ok \\ \hline
    
    Underflows sur les floats
    & Détection des underflow
    & \ok \\[.5cm] \hline
    
    Inf et NaN sur les floats
    & Détection des passages aux valeurs infinis et NaN sur les additions et soustractions
    & \ok \\ \hline
\end{longtable}

\subsection{Wrappers}
Cette section décrit les tests réalisés sur les types wrappers.

Ces premiers tests sont des tests généraux réalisés sur les deux wrappers BigFloat et BigInt
\begin{longtable}{ | p{.30\textwidth} | p{.60\textwidth}  | c | }
    \hline
    \textbf{Nom du test} & \textbf{Description} & \textbf{Visa} \\ \hline
 
    Arithmetic Operators
    & Les opérateurs arithmétiques retournent un Wrapper et fonctionnent lorsqu'ils sont exécutés sur des Wrappers
    & \ok \\ \hline
    
    Unary operators
    & Les opérateurs unaires + et - retournent un Wrapper et fonctionnent lorsqu'ils sont exécutés sur des Wrappers
    & \ok \\ \hline
    
    Comparison operators
    & Les opérateurs de comparaisons fonctionnent lorsqu'ils sont exécutés sur des Wrappers
    & \ok \\ \hline
    
    Complex operations
    & Les opérateurs arithmétiques retournent un Wrapper complexes et fonctionnent
    lorsqu'ils sont exécutés sur des Wrappers complexes
    & \ok \\ \hline
    
    Matrix operations
    & Les opérateurs arithmétiques retournent une matrice de wrapper et fonctionnent
    lorsqu'ils sont exécutés sur des matrices de wrapper
    & \ok \\ \hline
    
    General function call
    & Une fonction avec un argument de type suffisamment générique est appelée correctement lorsqu'on y passe un
    wrapper en argument
    & \ok \\ \hline
    
    Too specific function
    & Une fonction avec un argument de type trop spécifique est appelée correctement lorsqu'on y passe un
    wrapper en argument, celui-ci est unwrappé avec perte
    & \ok \\ \hline
    
    Too specific function with lossless unwrap
    & Une fonction avec un argument de type trop spécifique mais appelable sans perte est appelée correctement
    lorsqu'on y passe un wrapper en argument, celui-ci est unwrappé sans perte
    & \ok \\ \hline
    
    Both function without strict mode
    & Si une fonction avec un argument de type trop spécifique ayant une implémentation avec perte et une autre
    sans perte est appelée avec un wrapper, c'est la fonction sans perte qui est prioritaire si le mode strict
    est désactivé
    & \ok \\ \hline
    
    Both function with strict mode
    &  Si une fonction avec un argument de type trop spécifique ayant une implémentation avec perte et une autre
    sans perte est appelée avec un wrapper, c'est la fonction avec perte qui est prioritaire si le mode strict
    est activé
    & \ok \\ \hline
    
    Too specific function with array
    & Une fonction avec un argument de type trop spécifique est appelée correctement lorsqu'on y passe un
    tableau de wrappers en argument, celui-ci est unwrappé avec perte
    & \ok \\ \hline
    
    Simple generic struct
    & Une structure avec un champ de type suffisamment générique est créée correctement lorsqu'on y passe un
    wrapper en argument
    & \ok \\ \hline
    
    Too specific struct
    & Une structure avec un champ de type trop spécifique est créée correctement lorsqu'on y passe un
    wrapper en argument, celui-ci est unwrappé avec perte
    & \ok \\ \hline
    
    Too specific struct with array
    & Une structure avec un champ de type trop spécifique est créée correctement lorsqu'on y passe un
    tableau de wrappers en argument, celui-ci est unwrappé avec perte
    & \ok \\ \hline
    
    Simple parameterized struct
    & Une structure avec un paramètre de type suffisamment générique est créée correctement lorsqu'on y passe un
    tableau de wrappers en paramètre
    & \ok \\ \hline
    
    Too specific parameterized struct
    & Une structure avec un paramètre de type trop spécifique est créée correctement lorsqu'on y passe un
    tableau de wrappers en paramètre, celui-ci est unwrappé avec perte
    & \ok \\ \hline
    
    Too specific local variable
    & Une variable locale de type trop spécifique est créée correctement lorsqu'on y associe un wrapper,
    celui-ci est unwrappé avec perte
    & \ok \\ \hline
    
    Generic local array
    & Lorsqu'un tableau est créé avec des variables wrappables, celles-ci sont immédiatement wrappées
    & \ok \\ \hline
    
    Too specific local array
    & Lorsqu'un tableau est créé avec des variables wrappables en y précisant le type, celles-ci sont immédiatement
    wrappées et le type du tableau également
    & \ok \\ \hline
    
    Generic local matrix
    & Lorsqu'une matrice est créée avec des variables wrappables, celles-ci sont immédiatement wrappées
    & \ok \\ \hline
    
    Too specific local matrix
    & Lorsqu'une matrice est créée avec des variables wrappables en y précisant le type, celles-ci sont immédiatement
    wrappées et le type du tableau également
    & \ok \\ \hline

\end{longtable}
\subsubsection{Wrapper BigFloat}
Ces tests sont spécifiques au wrapper BigFloat.

\begin{longtable}{ | p{.30\textwidth} | p{.60\textwidth}  | c | }
    \hline
    \textbf{Nom du test} & \textbf{Description} & \textbf{Visa} \\ \hline
         
    Divisions on integers
    & Les opérateurs de divisions retournent un BigFloatWrapper mêmes lorsqu'ils sont exécutés sur des entiers
    & \ok \\ \hline
    
    Arithmetic operators with integers
    & Les opérateurs arithmétiques retournent un BigFloatWrapper lorsqu'ils sont exécutés ente un Float et un entier
    & \ok \\ \hline   

\end{longtable}

\subsubsection{Wrapper BigInt}
Ces tests sont spécifiques au wrapper BigInt.
\begin{longtable}{ | p{.30\textwidth} | p{.60\textwidth}  | c | }
    \hline
    \textbf{Nom du test} & \textbf{Description} & \textbf{Visa} \\ \hline
    
    Bitwise operators without bitwise unwrap
    & Lorsqu'un opérateur bitwise est appelé sur des opérandes de type BigIntWrapper, celles-ci ne sont pas unwrappées
    avant d'effectuer l'opération si le mode "bitwiseUnwrap" est désactivé
    & \ok \\ \hline
    
    Bitwise operators with bitwise unwrap
    & Lorsqu'un opérateur bitwise est appelé sur des opérandes de type BigIntWrapper, celles-ci sont unwrappées
    avant d'effectuer l'opération si le mode "bitwiseUnwrap" est activé
    & \ok \\ \hline

\end{longtable}