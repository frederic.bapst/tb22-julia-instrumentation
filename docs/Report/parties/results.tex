Ce chapitre décrit le système de configuration de notre outil mais aussi les différents programmes de démonstration
qui ont été réalisés. C'est également ici que l'on retrouve les résultats des tests de performance ainsi qu'une liste
des limitations de notre programme.

\section{Configuration}
Un objet de configuration peut être passé à la macro \inline{@cojac} afin d'en modifier le comportement, le tableau
suivant énumère les paramètres disponibles.

\begin{longtable}{ | p{.32\textwidth} | p{.40\textwidth} | p{.18\textwidth} | }
    \hline
    \textbf{Paramètre} & \textbf{Description} & \textbf{Valeur par défaut} \\ \hline
         
    check.enabled
    & Active la détection des anomalies
    & true \\ \hline
    
    check.cancellationThreshold
    & Détermine le seuil à partir duquel une opération entre deux nombres déclenche une anomalie d'annulation.
    Plus la valeur est petite, plus les nombres doivent être proches pour la déclencher.
    & 32 \\ \hline
 
    check.comparisonThreshold
    & Détermine le seuil à partir duquel une comparaison entre deux nombres déclenche une anomalie.
    Plus la valeur est petite, plus les nombres doivent être proches pour la déclencher.
    & 32 \\ \hline
 
    check.ignoredFunctions
    & Liste de fonctions dont les opérations internes ne déclenchent pas d'anomalies. Généralement utilisé
    pour les fonctions qui créent des anomalies dans leur fonctionnement normal.
    & [println, sleep] \\ \hline
 
    reaction.log
    & Active le logging des anomalies détectées
    & true \\ \hline

    reaction.report
    & Active le rapport final des anomalies détectées
    & false \\ \hline

    reaction.graph.enabled
    & Active le graphe de stacktraces des anomalies détectées
    & false \\ \hline

    reaction.graph.file
    & Détermine le fichier dans lequel est sauvegardé le graphe.
    & "anomalies.png" \\ \hline

    reaction.graph.size
    & Détermine la taille de l'image du graphe.
    & (1000, 1000) \\ \hline

    wrappers.strictMode
    & Active le mode strict sur les wrappers
    & false \\ \hline

    wrappers.checkUnwraps
    & Active l’émission d'une anomalie lorsqu'un wrapper doit être unwrappé
    & true \\ \hline

    wrappers.ignoredModules
    & Liste les modules dont les opérations internes ne sont jamais wrappées ou unwrappées
    & [Core, Base] \\ \hline

    wrappers.wrapFunctions
    & Liste les fonctions qui déclenchent un wrapping sur tous leurs arguments
    & Tableau de plusieurs fonctions\footnote{Précisément, tous les opérateurs arithmétiques, les opérateurs bitwise,
    ainsi que les fonctions de création de vecteurs et de matrices} \\ \hline

    wrappers.blank
    & Active le wrapper Blank
    & false \\ \hline

    wrappers.bigInt.enabled
    & Active le wrapper BigInt
    & false \\ \hline

    wrappers.bigInt.bitwiseUnwrap
    & Active l'unwrapping des opérandes avant une opération bitwise
    & true \\ \hline

    wrappers.bigFloat.enabled
    & Active le wrapper BigFloat
    & false \\ \hline

    wrappers.bigFloat.precision
    & Détermine la précision des calculs sur le wrapper BigFloat
    & 255 \\ \hline

\end{longtable}

\section{Démonstrations}
En plus des tests unitaires décrits dans le chapitre \ref{chap:tests}, deux petits programmes de démo
ont été réalisés.

\subsection{Démonstration des anomalies}
Il s'agit d'une démonstration déclenchant volontairement plusieurs centaines de fois chaque type d'anomalie.
Elle permet de montrer le fonctionnement des différents types de rapports d'anomalies. De plus, elle peut-être
lancée en utilisant plusieurs threads afin de montrer que les anomalies sont détectées correctement même en utilisant
la programmation concurrente.

\subsection{Démonstration des wrappers}
Cette démonstration se concentre sur l'utilité des wrappers BigInt et BigFloat en montrant les résultats de divers
calculs d'abord sans, puis avec les wrappers. La figure \ref{fig:demoWrappers} montre le résultat obtenu en lançant
démo.

\begin{figure}[H]
    \begin{center}
        \begin{textcode}
            Without Wrappers :
            pi = 3.141592653589793
            10^20 = 7766279631452241920
            muller(50) = 100.0
            ------------------
            With Wrappers :
            pi = 3.141592653589793115997963468544185161590576171875
            10^20 = 100000000000000000000
            muller(50) = 4.9999999999[...]
            ------------------
        \end{textcode}
      \caption{Résultat de la démo des wrappers BigInt et BigFloat}
      \label{fig:demoWrappers}
    \end{center}
\end{figure}

Le dernier exemple de cette démonstration est basé sur la fonction récursive suivante :
$$M(x_n) = 108 - \frac{815-\frac{1500}{x_{n-1}}}{x_{n-2}}$$ avec $x_0=4$ et $x_1=4.25$.
Il s'agit d'une version modifiée par William Kahan\cite{kahan-recurrence} d'une fonction décrite pour la première fois
par Jean-Michel Muller. Cette fonction a pour particularité de tendre vers 5 lorsque n tend vers l'infini mais les
erreurs successives d'arrondis finissent irrémédiablement par l'éloigner de cette valeur pour
tendre vers 100. Il s'agit donc d'un exemple particulièrement frappant de la différence de
résultat que l'on peut obtenir lorsque l'on utilise des variables ayant une meilleure précision.

\section{Performances}
Pour tester l'impact de l'outil sur les performances, on lance le programme de démonstration de détection
d'anomalies avec plusieurs configuration de Cojac, le tout en utilisant la macro \inline{@time} qui permet de
calculer le temps que prend l’exécution d'une expression ainsi que le nombre d'allocations crées sur le heap.

Le programme testé ne fait qu'une dizaine de milliers de calcul et le temps d'exécution devrait être de l'ordre de la
microseconde, ce qui rendrait nos calculs imprécis mais la figure \ref{fig:perfs} montre des différences suffisamment
élevées qu'il n'a pas été jugé utile de tester de plus gros programmes.

\begin{fig}{perfs}{Résumé de l'impact de l'outil sur les performances d'un petit programme}
    \begin{longtable}{ | c | c | c | >{\centering\arraybackslash}p{.15\textwidth} | }
        \hline
        \textbf{Configuration} & \textbf{Avec compilation} & \textbf{Sans compilation} & \textbf{x10} \\ \hline
             
        Sans COJAC
        & \SI{35}{\micro\second}
        & \SI{35}{\micro\second}
        & \SI{345}{\micro\second}
        \\ \hline

        Wrappers et anomalies désactivés
        & \SI{4.08}{\second}
        & \SI{1.39}{\second}
        & \SI{14}{\second}
        \\ \hline   

        Anomalies activées
        & \SI{13.82}{\second}
        & \SI{11.03}{\second}
        & \SI{112}{\second}
        \\ \hline   

        Wrapper Blank activé
        & \SI{1.83}{\second}
        & \SI{0.18}{\second}
        & \SI{1.84}{\second}
        \\ \hline   

        Wrapper BigFloat activé
        & \SI{1.78}{\second}
        & \SI{0.20}{\second}
        & \SI{2.08}{\second}
        \\ \hline   

        Wrapper BigInt activé
        & \SI{1.85}{\second}
        & \SI{0.24}{\second}
        & \SI{2.24}{\second}
        \\ \hline   

        Wrappers et anomalies activés
        & \SI{8.72}{\second}
        & \SI{6.45}{\second}
        & \SI{64}{\second}
        \\ \hline   
    
    \end{longtable}
\end{fig}

Cette analyse montre en effet que même en désactivant tous les wrappers ainsi que la détection d'anomalies,
on monte rapidement à des temps bien trop élevés pour être acceptables.

Plus étonnant encore: on obtient des temps plus élevés sans wrappers qu'avec.
Ceci n'est pas du à une quelconque optimisation de Julia due au fait qu'on relance à chaque fois le même programme
puisque chaque analyse est faite séparément en relançant à chaque fois entièrement le programme.

\subsection{Analyse des résultats}
L'analyse de performance ayant été effectuée assez tard dans le projet, il n'a pas été possible de comprendre exactement
les raisons d'un tel résultat. Quelques pistes peuvent néanmoins être évoquées:

\begin{itemize}
    \item Des fonctions trop génériques: Lorsque une fonction est générique, Julia peut la compiler une fois
    pour chaque combinaison de types de variables donnés. Notre outil possède de nombreuses fonctions appelées
    à chaque instruction avec beaucoup de types différents, pouvant résulter en un très grand nombre de version
    de ces fonctions compilés.
    \item Changement de types des variables: La documentation de Julia\cite{julia-perfs} indique que c'est une mauvaise
    idée de donner à une même variables plusieurs types différents au fil du programme, c'est pourtant ce qui est
    fait notamment avec la liste des arguments dans la fonction \inline{wrapFunction}.
    \item Instrumentation des opérateurs: En Julia, les opérateurs arithmétiques sont plus complexes que de simplement
    effectuer le calcul, il est possible que le fait d'instrumenter l'intérieur de ces opérateurs, qui sont utilisés
    très souvent, même par les fonctions internes à Julia, augmente significativement l'impact sur les performances.
    Ceci expliquerait notamment pourquoi l'impact est plus grand sans wrappers puisque lors de l'utilisation de ceux-ci,
    on appelle directement la fonction de wrapping à la place de l'opérateur, sans l'instrumenter.
\end{itemize}

La documentation de Julia possède en outre une page dédiée à la résolution de ce genre d'incidents\cite{julia-perfs}.
Mais la nature de notre programme étant particulière, il n'est pas aisé de reconnaître où se trouve le problème.
Il serait toutefois judicieux de mieux comprendre et résoudre ces problèmes de performances avant de songer à ajouter
des fonctionnalités à cet outil.

\section{Limitations}\label{results:limit}
Outre les problèmes de performances décrits dans la section précédente, cette implémentation possède quelques
limitations qu'il est nécessaire d'évoquer.

\subsection{Combinaison de plusieurs wrappers}
Comme aucune fonction n'a été créée pour gérer conversion entre wrappers, il est pour l'instant impossible
de combiner plusieurs wrappers entre eux sous peine de très vite voir apparaître des erreurs indiquant que Julia
ne peut pas effectuer la conversion de l'un à l'autre. Pour régler ce problème il faudra ainsi écrire des fonctions
de conversion entre les différents wrappers.

\subsection{Fonctions et modules ignorés}
Le système permettant d'ignorer les fonctions lors des détections d'anomalies et d'ignorer les modules pour les wrappers
manque de finesse. Il serait intéressant de pouvoir choisir d'ignorer tout un module pour le premier, ou juste quelques
fonctions pour le second. Mais également de pouvoir choisir d'ignorer une signature particulière de fonction seulement.
Ainsi, on pourrait choisir de détecter les anomalies dans la fonction \inline{f(::Integer)} mais pas dans 
\inline{f(::Number)}.

\subsection{Choix des anomalies à détecter}
Toujours au niveau de la finesse de la configuration, il serait intéressant de pouvoir choisir précisément quels types
d'anomalies on souhaiterait détecter au lieu d'un simple interrupteur on/off permettant soit de tout détecter soit
de ne rien détecter.

\subsection{Définition des stackframes intéressantes}
Pour détecter si une stackframe est intéressante à montrer à l'utilisateur lors des différents types de rapports
d'anomalies, l’approche actuelle consiste à se baser uniquement sur le nom du fichier de code source de la stackframe et
de blacklister certaines chaînes de caractères. Il s'agit d'un système relativement peu fiable et pouvant être sujet
à de nombreux faux positifs. Dans l'idéal, il faudrait pouvoir récupérer le "module" d'où vient la fonction pour
déterminer plus efficacement si celle-ci a été écrite par l'utilisateur ou non mais les stackframes ne permettent pas
d'obtenir cette information.

\subsection{Keyword Arguments}
En Julia, il est possible pour chaque fonction, en plus des arguments classiques, de donner des arguments nommés, tel
que \inline{f(Myanmar=value)}. Lors de la déclaration de la fonction, ceux-ci sont séparés des arguments classiques 
à l'aide d'un point-virgule (\inline{function f(arg1, arg2; kw1, kw2)}). Ces arguments sont gérés de manière différente
que les arguments classiques et nécessite donc une attention particulière.

En particulier, le système permettant d'unwrapper les arguments au besoin pour appeler une fonction demandant un
type spécifique ne s’intéresse qu'aux arguments classiques. La figure \cite{fig:kw} montre un programme dont l’exécution
provoquera une erreur du fait que le keyword argument de la fonction \inline{f} n'est pas unwrappé.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            function f()
                n = 3+4
                return g(val=n)
            end
            function g(;val)
                return val
            end
            config = Config()
            config.wrappers.bigInt.enabled = true
            @cojac config f()
        \end{juliacode}
      \caption{Exemple de programme posant problème à cause des keyword arguments}
      \label{fig:kw}
    \end{center}
\end{figure}

Régler se problème nécessiterait donc de gérer correctement ce genre d'arguments, notamment au niveau du système
d'unwrapping.


\subsection{Fragilité de l'approche}
Certaines limitations sont inhérentes à l'approche choisie et nécessiteraient de changer complètement celle-ci pour
espérer être corrigées.

\subsubsection{Méthodes à signature abstraite}
Certaines fonctions de la bibliothèque de base de Julia possèdent des implémentations possédant une signature
abstraite mais pouvant renvoyer une erreur. C'est par exemple le cas des opérateurs arithmétiques qui
possèdent chacun une implémentation du type \inline{+(a::Number, b::Number)}. Une telle fonction appelée avec des
wrappers, qui dérivent du type Number va ainsi lancer une exception indiquant qu'elle n'est pas définie pour notre type
Wrapper.

Cela pose un problème puisque notre implémentation utilise la fonction \inline{applicable} qui indique si une
implémentation d'une fonction donnée existe pour le type des arguments choisis. Or, ici la fonction existe bien
mais elle lance une exception indiquant qu'elle n'existe pas.

Ce n'est pas un problème pour les opérateurs arithmétiques puisque nous les implémentons de toute façon spécifiquement
pour le type de nos wrappers mais il n'est pas exclu qu'il existe d'autres fonctions du même genre.

\subsubsection{Bugs et spécificités de Cassette et Julia}
Au cours de l'implémentation, il a été découvert certaines spécificités et bugs ayant nécessité des solutions
de contournement (fonctions BuiltIn, tuples vides, promote\_op, etc.). Il est tout à fait possible que d'autres
problèmes du genre puissent survenir lors de l'utilisation de l'outil sur des programmes plus conséquent Il faudra
alors probablement ajouter de nouvelles solutions pour contourner lesdits problèmes.