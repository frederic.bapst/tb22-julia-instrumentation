Ce chapitre décrit la la façon dont sera utilisé l'outil ainsi que les différentes fonctionnalités qui seront
implémentées.

\section{Utilisation de l'outil}
L'outil mettra à disposition une macro "@cojac" qui permettra d'inspecter le code de l'expression qu'on lui donne.
Un objet de configuration pourra également lui être passé. La figure \ref{fig:usecase} donne un exemple d'utilisation
typique qui pourra être fait.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
        # On importe Cojac ainsi que notre programme à inspecter
        using Cojac: @cojac, Config
        using MyModule: main

        # On crée un objet de configuration
        config = Config()
        config.reaction.report = true
        config.wrappers.bigFloat.enabled = true
        config.wrappers.bigFloat.precision = 500

        # On peut appeler cojac soit avec la configuration par défaut,
        # soit avec une config personnalisée
        @cojac main()
        @cojac config main()
        \end{juliacode}
      \caption{Exemple de cas d'utilisation}
      \label{fig:usecase}
    \end{center}
\end{figure}

\section{Détection d'anomalies}
La première fonctionnalité à implémenter est la détection d'anomalies. Les anomalies sont des irrégularités de certains
calculs pouvant être signe d'un dysfonctionnement du code source, c'est pourquoi il est intéressant de les remonter
à l'utilisateur pour qu'il puisse, au besoin, les corriger.

\subsection{Anomalies détectées}
Voici les différentes anomalies que devra détecter notre outil, en plus de les détecter sur des calculs de nombres
"classiques", le mécanisme devra     également fonctionner pour les nombres complexes ainsi que pour les calculs matriciels.

\subsubsection{Integer Overflows}
Il s'agit d'une anomalie présente sur les types de nombres entiers. Elle apparaît lorsqu’une opération dépasse
la valeur maximale (ou minimale) du type choisi, donnant un résultat erroné.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            r = 2^62 * 10 # r = -9223372036854775808
        \end{juliacode}
      \caption{Exemple d'anomalie de type overflow}
    \end{center}
\end{figure}

\subsubsection{Absorptions}
Il s'agit d'une anomalie présente sur les types de nombres à virgule flottante. Elle apparaît lorsque les deux opérandes
d'une addition ou d'une soustraction ont un ordre de grandeur trop éloigné. L'opérande ayant le plus petit ordre de
grandeur se trouve ainsi "absorbée" par l'autre, comme si elle valait zéro.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            r = 1e50 + 1.0 # r = 1e50
            r = 1e-50 - 1.0 # r = -1.0
        \end{juliacode}
      \caption{Exemples d'anomalie de type absorption}
    \end{center}
\end{figure}

\subsubsection{Annulations}
Il s'agit d'une anomalie présente sur les types de nombres à virgule flottante. Elle apparaît lorsque les deux opérandes
d'une soustraction ou d'une addition sont tellement proches que la différence pourrait venir d'une erreur d'arrondi,
mais la soustraction retourne du bruit au lieu de zéro. Un seuil permettra de qualifier ce qui est considéré comme 
"très proches".

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            r = 1.000000000000001 - 1.0 # r = 1.1102230246251565e-15
        \end{juliacode}
      \caption{Exemple d'anomalie de type annulations}
    \end{center}
\end{figure}

\subsubsection{Comparaisons discutables}
Il s'agit d'une anomalie présente sur les types de nombres à virgule flottante. Elle apparaît lorsque les deux opérandes
d'une comparaison sont tellement proches que la différence pourrait venir d'une erreur d'arrondi, 
la comparaison retourne ainsi que l'une est plus grande que l'autre, alors que leurs valeurs exactes théoriques seraient
les mêmes. Tout comme pour les annulations, un seuil permettra de qualifier à partir de quel moment les opérandes sont
trop proches.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            r = 1.00000000001 > 1 # r = true
            r = 1.00000000001 == 1 # r = false
        \end{juliacode}
      \caption{Exemple d'anomalie de type comparaison discutable}
    \end{center}
\end{figure}


\subsubsection{Underflows}
Il s'agit d'une anomalie présente sur les types de nombres à virgule flottante. Elle apparaît lorsque le numérateur
d'une division ou d'une multiplication est beaucoup plus petit que son dénominateur. Le résultat de la division est
donc arrondi à zéro.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            r = 1e-200 / 1e200 # r = 0.0
        \end{juliacode}
      \caption{Exemple d'anomalie de type underflows}
    \end{center}
\end{figure}

\subsubsection{Float Overflows / NaN}
Il s'agit d'une anomalie présente sur les types de nombres à virgule flottante. Elle apparaît lorsqu'un calcul effectué
dépasse la valeur maximale du type choisi, le résultat prend donc la valeur "Inf" ou "-Inf". La valeur not-a-number
quant à elle apparaît lorsque l'on effectue des calculs impossibles, typiquement 0/0. 

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            r = 1e200 ^ 2 # r = Inf
            r = 0 / 0 # r = NaN
        \end{juliacode}
      \caption{Exemples d'anomalies de type Float Overflow et NaN}
    \end{center}
\end{figure}

\subsection{Rapports d'anomalies}\label{conception:reports}

Une fois détectées, les anomalies pourront être rapportées de trois manières différentes.

\subsubsection{Logging}
Le premier type de rapport est celui choisi par défaut. Il log simplement l'information qu'une anomalie a été détecté
dans la console. En plus du type d'anomalie, il indique également le fichier dans lequel celle-ci a été commise.

\subsubsection{Rapport final}
On peut également présenter les anomalies sous forme de rapport affiché à la fin du programme. Celui-ci
indique pour chaque type d'anomalies la liste des endroits où celle-ci a été détectée ainsi que le nombre de fois 
que l'anomalie a été détectée à cet endroit.

\subsubsection{Graphe de stacktrace}
Le troisième type de rapport sera sous forme d'un graphe représentant les stacktraces complètes de chaque
execution ayant généré des anomalies. Le graphe indiquera également le nombre de fois que
l'anomalie a été détectée à cet endroit.

\section{Wrappers}
Le système de wrapper consiste à transformer les types numériques par des objets permettant ainsi d'en modifier
le comportement. Durant ce projet, trois wrappers différents ont été développés, d'autres wrappers possibles sont
discutés dans les développements futurs, section \ref{future:wrappers}.

\subsection{Wrapper Blank}
Il s'agit d'un wrapper de test. Il ne fait rien d'autre que de transformer tous les types de nombres réels en objet
contenant ledit nombre. Il est ainsi utilisé pour mesurer l'impact de l'utilisation des wrappers sur un programme donné. 

\subsection{Wrapper BigFloat}
Ce wrapper consiste à remplacer tous les nombres à virgule flottante par des valeurs de type "BigFloat"
pouvant contenir n'importe quel nombre avec une précision arbitraire. La précision voulue lors de l'utilisation du
wrapper pourra être paramétrée dans la configuration.

\subsection{Wrapper BigInt}
Ce wrapper consiste à remplacer tous les nombres entiers par des valeurs de type "BigInt" pouvant contenir n'importe
quel nombre entier sans limite maximale ou minimale. Il n'y a ainsi plus de problème d'integers overflows.

\subsubsection*{Opérateurs bitwise}
Les opérateurs bitwise n'agissant pas sur la valeur mathématique réelle des opérandes mais sur leurs bits, le remplacement
de celles-ci par des BigInt ayant un nombre variable de bits peut poser problème. C'est pourquoi un paramètre dans la
configuration permettra de reconvertir le nombre dans son type initial avant l'application de ces opérateurs.