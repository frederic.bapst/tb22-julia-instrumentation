Ce chapitre décrit l'analyse des différentes technologies utilisées dans ce projet.

\section{Julia}
Cette section décrit les quelques spécificités du langage Julia sur lesquelles il est utile de s'attarder pour ce
projet. Les informations citées ici sont retrouvables dans la documentation officielle du langage\cite{julia-doc}.

\subsection{Le compilateur Julia}
En Julia, la compilation se fait just-in-time. C'est-à-dire qu'excepté certaines fonctions internes au langage, chaque
fonction est compilée juste avant son exécution. De plus, il est possible d'avoir accès à une représentation intermédiaire
du langage afin d'y apporter des modifications, notamment à l'aide de macros. C'est ce système qui est utilisé par des 
librairies comme Cassette.

\subsection{Représentation des nombres}
Julia possède plusieurs types numériques permettant de représenter différentes catégories de nombres.
Comme le résume la figure \ref{fig:numericTypes}, ces types sont organisés sous forme de hiérarchie.
Ainsi, lorsque l'on écrit une fonction, il est fortement conseillé d'être le plus général possible et de ne pas "forcer"
à utiliser par exemple des Int64 si notre fonction pourrait accepter tout type de nombre entier.

\img{numericTypes}{Hiérarchie des types numériques en Julia}

\subsubsection{Nombres entiers}
Julia possède deux types de nombres entiers : les types signés et les types non-signés.
Chacun de ces types possède des déclinaisons à 8, 16, 32, 64 et 128 octets. De plus, un type \inline{BigInt} permet de
gérer des nombres entiers aussi grands que l'on veut.

\subsubsection{Nombres réels}
Comme pour les entiers, il existe plusieurs types de nombres réels, chacun ayant une précision différente.
Il s'agit des représentations sur 16 octets (half precision), 32 octets (single precision) et 64 octets
(double precision). Un type \inline{BigFloat} permet également d'obtenir une précision arbitraire.

\subsubsection{Nombres irrationnels}
Le type irrationnel possède un statut particulier. Il s'agit du type attribué aux constantes mathématiques 
intégrées à Julia, comme $\pi$ ou $e$. À savoir que les valeurs dérivées de ces constantes perdent le type irrationnel,
Ainsi, $3*\pi$ est de type \inline{Float} et non pas \inline{Irrational}.

\subsubsection{Types dérivés}
En plus de tous ces types, Julia gère les nombres complexes et les nombres rationnels. Il s'agit de types
pouvant être dérivés de ceux décrits ci-dessus. On peut donc parler de \inline{Complex\{Float32\}}, de
\inline{Rational\{UInt16\}}, ou même de \inline{Complex\{Rational\{BigInt\}\}}. En revanche, les nombres rationnels
ne peuvent se dériver qu'à partir de types entiers.

\paragraph*{Nombres complexes}
Les nombres imaginaires se créent à partir de la constante \inline{im} qui représente le nombre $i$.
On peut donc effectuer les calculs usuels sur les nombres complexes comme décrit par la figure \ref{fig:complexExample}.

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        julia> im*im
            -1 + 0im
        julia> (3+5im) * 2im
            -10 + 6im
    \end{juliacode}
      \caption{Exemple de calculs sur les nombres complexes}
      \label{fig:complexExample}
    \end{center}
\end{figure}

\paragraph*{Nombres rationnels}
Les nombres rationnels sont créés à partir de l'opérateur \inline{//} et de deux nombres entiers. Ils ont l'avantage
de toujours représenter une valeur exacte et de se simplifier automatiquement (cf. figure \ref{fig:rationalExample}).
Les dépassements d'entiers sont particulièrement difficiles à prédire sur ce type de nombre. Une erreur de type
\inline{OverflowError} est lancée par Julia lorsque le numérateur ou le dénominateur d'un nombre rationnel devrait
dépasser la limite du type utilisé.

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        julia> 6//4
            3//2
        julia> 3//2 + 8//3
            25//6
        julia> (3//2) ^ 40
            ERROR: OverflowError [...]
    \end{juliacode}
      \caption{Exemple de calculs sur les nombres rationnels}
      \label{fig:rationalExample}
    \end{center}
\end{figure}

\subsection{Fonctions et méthodes}
En Julia, une "fonction" est un nom générique pouvant être ensuite implémenté par plusieurs "méthodes" avec une signature
différentes dans la figure \ref{fig:funcMeth}, la fonction "f" possède deux méthodes \inline{f(::Number)} et
\inline{f(::Int64)}

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        function f(a::Number)
            return a + 2
        end
        function f(a::Int64)
            return a + 3
        end
    \end{juliacode}
      \caption{Deux méthodes définies pour la même fonction "f"}
      \label{fig:funcMeth}
    \end{center}
\end{figure}

\subsubsection*{Multiple dispatch}
En Java comme dans la plupart des langages de programmation orienté objets, le choix de la méthode à exécuter dépend du
type du premier argument de la méthode (il s'agit en réalité de l'objet auquel appartient la méthode), il s'agit du
single dispatch.

Julia n'est pas exactement un langage orienté objet car les fonctions ne peuvent pas appartenir à des objets. À la
place, il y a un concept de multiple dispatch, c'est-à-dire que le choix de la méthode à exécuter dépend du type
effectif (dynamique) de chaque argument qui lui est passé. Il s'agit donc de quelque chose de bien différent de l'overloading
qui ne dépend que du type déclaré (typage statique) des arguments. 

La figure \ref{fig:multipleDispatch} illustre cette fonctionnalité à l'aide d'exemples.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
            function f(a::Signed, b::Number)
                println("Signed Number")
            end

            function f(a::Signed, b::Unsigned)
                println("Signed Unsigned")
            end

            function f(a::Number, b::Signed)
                println("Number Signed")
            end

            a::Number = Int32(3)
            b::Number = Float16(3.0)
            c::Number = UInt32(3)

            f(a, b)
            f(b, a)
            f(a, c)
            f(a, a)
        \end{juliacode}
        \begin{textcode}
            Signed Number
            Number Signed
            Signed Unsigned
            ERROR: LoadError: MethodError: f(::Int64, ::Int64) is ambiguous. Candidates:
              f(a::Signed, b::Number) in Main at dispatch.jl:3
              f(a::Number, b::Signed) in Main at dispatch.jl:11
            Possible fix, define
              f(::Signed, ::Signed)
        \end{textcode}
      \caption{Le multiple dispatch en Julia}
      \label{fig:multipleDispatch}
    \end{center}
\end{figure}

La dernière invocation lance une erreur. En effet il n'est pas possible de choisir entre les deux méthodes car aucune
n'est plus "précise" que l'autre dans les types de ses arguments.

\subsection{Types}
En Julia, bien que les types soient dynamiques, il est possible d'annoter une variable ou un argument de fonction
pour en déclarer le type à l'aide de l'opérateur "\inline{::}", par exemple : \inline{myVar::Float64 = 2.3}.

Chaque type peut également hérités d'autres type, cet héritage se note \inline{SubType <: SuperType}.
L'opérateur "\inline{<:}" est utilisé autant pour tester si un type est un sous-type d'un autre que pour
indiqué la hiérarchie lorsque l'on créé nos propres types.

\subsubsection{Structures}
Le principal moyen de créer des types en Julia est à l'aide du mécanisme de structures, semblables à ce que l'on peut
trouver en C par exemple.

Par défaut, toutes les structures ainsi créées sont immutables, pour créer des objets mutables, il faut ajouter le
mot-clef "mutable" devant la déclaration de la structure.

\paragraph*{Keyword Definition}
Par défaut, les champs d'une structure ne permettent pas de valeur par défaut. Ainsi, lorsque l'on crée un objet à
partir d'une structure, il faut obligatoirement renseigner tous les champs de celle-ci dans l'ordre où ils sont déclarés.

La macro Julia \inline{Base.@kwdef} permet de résoudre ces deux problèmes, on peut mettre des valeur par défaut aux champs
de nos structures et initialiser ceux qui nous intéressent à l'aide des keyword arguments.

La figure \ref{fig:structs} montre divers exemples de structures avec et sans keyword definitions.

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        mutable struct A <: Number
            value::Float64
        end

        Base.@kwdef mutable struct B
            value::Float64 = 6.2831
            id::Int64
        end

        a = A(1.618)
        b1 = B(id=1729)
        b2 = B(value=2.7182, id=42)
    \end{juliacode}
      \caption{Exemples de création d'objets à partir de structures}
      \label{fig:structs}
    \end{center}
\end{figure}

Cet exemple montre également comment indiquer que le type A est un sous-type de \inline{Number}.


\subsubsection{Types paramétrisés}
Chaque type peut également être paramétrisé à l'aide d'accolades. La figure \ref{fig:typeParams} montre plusieurs
exemples de paramétrisation.

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        struct A{T <: Number}
            value::T
        end

        function f(a::A{Integer})
        end

        function g(a::A{T}) where {T <: Integer}
        end
    \end{juliacode}
      \caption{Exemples de types paramétrisés et de leur utilisation dans des fonctions}
      \label{fig:typeParams}
    \end{center}
\end{figure}

Cet exemple introduit également l'utilisation du mot-clef "where". En effet, ici les signatures des fonction f et
g sont fondamentalement différentes car des types paramétrisés ayant des paramètres différents ne sont pas considérés
comme étant des sous-types l'un de l'autre. Ainsi, bien que \inline{Int64 <: Integer} est vrai, 
\inline{A\{Int64\} <: A\{Integer\}} est faux, ce qui signifie qu'appeler f avec un objet de type \inline{A\{Int64\}}
provoquera une erreur.

Le mot-clef "where" permet donc ici d'indiquer qu'on accepte n'importe quel type étant un sous-type de \inline{Integer},
ainsi que \inline{Integer} lui-même.

\subsection{Macros}
Le concept de macro est central en Julia. On reconnaît une macro d'une fonction classique par son préfixe "@".
Elles s'exécutent à la compilation et retournent une expression qui prend leur place lors de l'exécution.
Plus d'informations sur les macros sont disponibles dans la documentation\cite{julia-macros}.

Ainsi, lorsque l'on appelle \inline{@myMacro f(a,b,c)}, ce n'est pas le résultat de la fonction f que l'on passe
comme argument, mais bien l'expression de la fonction f sur les arguments a, b et c, sans que celle-ci ne soit exécutée.
On peut ensuite décider d'exécuter la fonction f dans l'expression retournée par la macro.

\subsection{Comparaisons avec Java}
Concernant les calculs, Julia possède quelques différences avec Java qu'il est nécessaire de relever.

\subsubsection{Opérateurs logiques et arithmétiques}
Julia étant un langage développé pour le calcul scientifique, il est naturel d'y trouver des opérateurs plus
spécifiques que ceux que l'on peut trouver en Java.
La figure \ref{fig:juliaOperators} décrit les opérateurs arithmétiques différents entre Julia et Java.

\begin{fig}{juliaOperators}{Différences entre les opérateurs Java et Julia}
        \begin{tabular}{ | c | p{6cm} | p{6cm} | }
            \hline
            \textbf{Opérateur} & \textbf{Java} & \textbf{Julia} \\ \hline
            \inline{x / y}
            & Division euclidienne lorsque les deux opérandes sont des entiers
            & Division retournant un Float même si les deux opérandes sont des entiers \\ \hline
            \inline{x ÷ y} & - & Division euclidienne (retourne un Float représentant un nombre entier si l'une des 
            opérande est un Float, par exemple \inline{7.1÷2 = 3.0}) \\ \hline
            \inline{x \textbackslash{} y} & - & Division inverse (équivalent à \inline{y/x}) \\ \hline
            \inline{x \^{} y} & bitwise XOR & Puissance \\ \hline
            \inline{x $\veebar$ y}  & - & bitwise XOR \\ \hline
            \inline{x $\barwedge$ y}  & - & bitwise NAND \\ \hline
            \inline{x $\triangledown$ y}  & - & bitwise NOR \\ \hline
        \end{tabular}
\end{fig}

L'opérateur le plus intéressant ici est celui de puissance qu'il sera notamment nécessaire de gérer pour les anomalies
de type overflow.


\subsubsection{Tableaux, vecteurs et matrices}
Contrairement à Java, Julia ne possède pas de tableaux à proprement parler, mais des vecteurs et des matrices.
On peut bien sûr créer des vecteurs de vecteurs comme pour les tableaux multidimensionnels de Java, mais l'intérêt
principal est de créer des matrices afin de pouvoir effectuer des calculs matriciels.
Les vecteurs se créent en séparant les valeurs par des virgules alors que les matrices sont créées en les séparant
par des espaces et des points virgules.
La figure \ref{fig:vectorsAndMatrices} montre quelques exemples de création de tels objets. 

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        julia> [1,2,3]
            3-element Vector{Int64}:
            1
            2
            3
        julia> [1 2 3]
            1×3 Matrix{Int64}:
            1  2  3
        julia> [1 2; 3 "a"]
            2×2 Matrix{Any}:
            1  2
            3  "a"
    \end{juliacode}
      \caption{Exemples de créations de vecteurs et de matrices}
      \label{fig:vectorsAndMatrices}
    \end{center}
\end{figure}

\break
L'intérêt de ces objets est de pouvoir faire du calcul vectoriel et matriciel.
Par exemple, l'opérateur de multiplication entre deux matrices effectuera un produit matriciel.
Comme pour d'autres langages à vocation scientifique (tel MATLAB), on peut précéder un opérateur par un point
pour le transformer en opérateur élément par élément. La figure \ref{fig:matrixOperations} montre quelques exemples
d'opérations sur les matrices.

\begin{figure}[H]
    \begin{center}
    \begin{juliacode}
        julia> [1 2 3] * [4; 5; 6]
            1-element Vector{Int64}:
            32
        julia> [1 2 3] .* [4  5  6]
            1×3 Matrix{Int64}:
            4  10  18
        julia> [1 2;3 4] ^ 2
            2×2 Matrix{Int64}:
            7  10
            15  22
    \end{juliacode}
      \caption{Opérations sur les matrices}
      \label{fig:matrixOperations}
    \end{center}
\end{figure}

\section{Cassette}
Cassette est une librairie Julia permettant de modifier le comportement du code intermédiaire Julia.

\subsection{Overdub et contexte}
Pour inspecter notre code, Cassette met à disposition la macro \inline{@overdub}. Celle-ci prend pour arguments un 
contexte (expliqué plus bas), ainsi qu'une expression sur laquelle sera réalisée l'inspection de code.
Cette expression sera typiquement l'appel à une fonction "main" de notre programme afin d'en inspecter tout le code.

On crée un type d'objet "context" à l'aide de la macro \inline{@context}. Ce contexte sera ensuite passé à notre 
macro overdub qui le passera à son tour à tous nos hooks. L'utilité principale de ce contexte et de pouvoir y passer
des "métadonnées", c'est-à-dire un objet du type que l'on veut qui sera passé à tous nos hooks.

\subsection{Hooks et override}
La fonctionnalité principale de Cassette et de pouvoir créer des hooks qui s'exécutent soit avant (prehook) soit après
(posthook) une instruction.

Ces fonctionnalités utilisent la puissance du multiple dispatch afin de pouvoir "choisir" sur quelles instructions
on veut appliquer nos hooks.

\break
En plus des hooks, on peut override directement la fonction "overdub" afin de modifier totalement le fonctionnement
d'une fonction voulue.

La figure \ref{fig:cassetteOverdub} montre un exemple de prehook, overdub et de posthook appliqués sur une fonction
simple afin d'en modifier le comportement.

\begin{figure}[H]
    \begin{center}
        \begin{juliacode}
# On créée notre contexte
Cassette.@context Ctx

# Les hooks prennent comme argument notre contexte, la fonction inspectée,
# ainsi que les arguments. Ici, on choisit donc de modifier le comportement
# de la fonction d'addition.
Cassette.prehook(::Ctx, ::typeof(+), a, b) = println("pre: $a + $b")

# En overridant la fonction "overdub", on modifie complètement
# ce que fait notre fonction
Cassette.overdub(::Ctx, ::typeof(+), a, b) = begin
    println("overdubbed")
    r = a - b
end

# Le posthook a également accès au résultat retournée par la fonction.
Cassette.posthook(::Ctx, res, ::typeof(+), a, b) = println("post: $a + $b = $res")

# Voici la fonction inspectée.
function f()
    r = 3 + 5
end

# On lance la fonction avec le système d'overdub de Cassette.
Cassette.@overdub Ctx() f()
        \end{juliacode}
        \begin{textcode}
            prehook: 3 + 5
            overdubbed
            posthook: 3 + 5 = -2
        \end{textcode}
      \caption{Modification de la fonction d'addition à l'aide de Cassette}
      \label{fig:cassetteOverdub}
    \end{center}
\end{figure}
